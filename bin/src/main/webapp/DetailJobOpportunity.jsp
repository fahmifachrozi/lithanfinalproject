<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
<title>ABC Jobs Portal - Welcome</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<script
	src="https://cdn.ckeditor.com/ckeditor5/23.0.0/inline/ckeditor.js"></script>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="css/style.css">
<link rel="stylesheet" href="css/card.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

</head>

<body>

	<s:if test="#session.isLoggedIn != null">
		<s:include value="/WEB-INF/header/header.jsp"/>
	</s:if>
	<s:else>
		<s:include value="/WEB-INF/header/LoginHeader.jsp"/>
	</s:else>
	
	<div class="container-fluid mt-3 mb-2">
		<div class="row">

			<div class="col-sm-12 col-md-10 col-lg-10" style="margin: auto;">
				<h3>Job Title</h3>
				<div class="card" style="width: 100%;">
					<div class="card-header">
						<img src="images/avatar.png" class="rounded-circle"
							style="width: 50px; float: left; margin-right: 10px;">

						<div>
							<span>Fahmi Fachrozi</span>
						</div>
						<div>
							<span>Indonesia</span>
						</div>
					</div>
					<div class="card-body"></div>
					<div class="card-footer">
						<p>Posted On:</p>
					</div>
				</div>
			</div>

		</div>
	</div>

	<!-- comment -->

	<div class="container-fluid">
		<div class="row ">


			<div class="col-lg-10" style="margin: auto;">
				<div class="col-lg-6 col-md-12 comment-main rounded">
					<ul class="p-0">
						<li>
							<div class="row comment-box p-1 pt-3 pr-4">
								<div class="col-lg-2 col-3 user-img text-center">
									<img src="images/avatar.png" class="main-cmt-img">
								</div>
								<div class="col-lg-10 col-9 user-comment bg-light rounded pb-1">
									<div class="row">
										<div class="col-lg-8 col-6 border-bottom pr-0">
											<p class="w-100 p-2 m-0">Lorem ipsum dolor sit amet.</p>
										</div>
										<div class="col-lg-4 col-6 border-bottom">
											<p class="w-100 p-2 m-0">
												<span class="float-right"><i
													class="fa fa-clock-o mr-1" aria-hidden="true"></i>01 : 00</span>
											</p>
										</div>
									</div>
									<div class="user-comment-desc p-1 pl-2">
										<p class="m-0 mr-2">
											<span><i class="fa fa-thumbs-up mr-1"
												aria-hidden="true"></i></span></i>490
										</p>
										<p class="m-0 mr-2">
											<span><i class="fa fa-thumbs-down mr-1"
												aria-hidden="true"></i></span></i>450
										</p>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-lg-11 offset-lg-1">
									<ul class="p-0">
										<li>
											<div class="row comment-box p-1 pt-2 pr-4">
												<div class="col-lg-3 col-3 user-img">
													<img src="images/avatar.png"
														class="p-2 float-right sub-cmt-img">
												</div>
												<div
													class="col-lg-9 col-9 user-comment bg-light rounded pb-1 mt-2">
													<div class="row">
														<div class="col-lg-7 col-6 border-bottom pr-0">
															<p class="w-100 p-2 m-0">Lorem ipsum dolor sit amet.</p>
														</div>
														<div class="col-lg-5 col-6 border-bottom">
															<p class="w-100 p-2 m-0">
																<span class="float-right"><i
																	class="fa fa-clock-o mr-1" aria-hidden="true"></i>01 :
																	00</span>
															</p>
														</div>
													</div>
													<div class="user-comment-desc p-1 pl-2">
														<p class="m-0 mr-2">
															<span><i class="fa fa-thumbs-up mr-1"
																aria-hidden="true"></i></span></i>510
														</p>
														<p class="m-0 mr-2">
															<span><i class="fa fa-thumbs-down mr-1"
																aria-hidden="true"></i></span></i>690
														</p>
													</div>
												</div>
											</div>
										</li>
									</ul>
								</div>
							</div>
							<div class="row">
								<div class="col-lg-11 offset-lg-1">
									<ul class="p-0">
										<li>
											<div class="row comment-box p-1 pt-2 pr-4">
												<div class="col-lg-3 col-3 user-img">
													<img src="images/avatar.png"
														class="p-2 float-right sub-cmt-img">
												</div>
												<div
													class="col-lg-9 col-9 user-comment bg-light rounded pb-1 mt-2">
													<div class="row">
														<div class="col-lg-7 col-6 border-bottom pr-0">
															<p class="w-100 p-2 m-0">Lorem ipsum dolor sit amet.</p>
														</div>
														<div class="col-lg-5 col-6 border-bottom">
															<p class="w-100 p-2 m-0">
																<span class="float-right"><i
																	class="fa fa-clock-o mr-1" aria-hidden="true"></i>01 :
																	00</span>
															</p>
														</div>
													</div>
													<div class="user-comment-desc p-1 pl-2">
														<p class="m-0 mr-2">
															<span><i class="fa fa-thumbs-up mr-1"
																aria-hidden="true"></i></span></i>120
														</p>
														<p class="m-0 mr-2">
															<span><i class="fa fa-thumbs-down mr-1"
																aria-hidden="true"></i></span></i>960
														</p>
													</div>
												</div>
											</div>
										</li>
									</ul>
								</div>
							</div>
						</li>
						<li>
							<div class="row comment-box p-1 pt-3 pr-4">
								<div class="col-lg-2 col-3 user-img text-center">
									<img src="images/avatar.png" class="main-cmt-img">
								</div>
								<div class="col-lg-10 col-9 user-comment bg-light rounded pb-1">
									<div class="row">
										<div class="col-lg-8 col-6 border-bottom pr-0">
											<p class="w-100 p-2 m-0">Lorem ipsum dolor sit amet.</p>
										</div>
										<div class="col-lg-4 col-6 border-bottom">
											<p class="w-100 p-2 m-0">
												<span class="float-right"><i
													class="fa fa-clock-o mr-1" aria-hidden="true"></i>01 : 00</span>
											</p>
										</div>
									</div>
									<div class="user-comment-desc p-1 pl-2">
										<p class="m-0 mr-2">
											<span><i class="fa fa-thumbs-up mr-1"
												aria-hidden="true"></i></span></i>109
										</p>
										<p class="m-0 mr-2">
											<span><i class="fa fa-thumbs-down mr-1"
												aria-hidden="true"></i></span></i>512
										</p>
									</div>
								</div>
							</div>
						</li>
						<hr>
						<div class="row">
							<div class="col-lg-10 col-10">
								<input type="text" class="form-control"
									placeholder="write comments ...">
							</div>
							<div class="col-lg-2 col-2 send-icon">
								<a href=""><i class="fa fa-paper-plane" aria-hidden="true"></i></a>
							</div>
						</div>
					</ul>

				</div>
			</div>
		</div>

		<!-- end of comment -->


		<div class="container-fluid bg-secondary text-white mt-5">
			<div class="row">
				<div class="col-sm-12 text-center p-4">
					<p>Copyright &copy; M. Fahmi Fachrozi</p>
				</div>
			</div>
		</div>
</body>
</html>