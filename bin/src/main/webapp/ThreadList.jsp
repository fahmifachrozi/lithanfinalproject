<!DOCTYPE html>
<html lang="en">
<head>
<title>ABC Jobs Portal - Welcome</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<script
	src="https://cdn.ckeditor.com/ckeditor5/23.0.0/inline/ckeditor.js"></script>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="css/style.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
<style>
.card {
	width: 25rem;
	text-align: left;
}

body {
	font-size: 14px;
}
</style>
</head>

<body>
	<nav class="navbar navbar-expand-sm bg-dark navbar-dark">
		<h2 class="navbar-brand text-white">
			<a href="profile.html" style="color: white; text-decoration: none;">
				<img src="images/logo.png" style="width: 50px; height: auto;">
				Jobs
			</a>
		</h2>

		<button class="navbar-toggler" type="button" data-toggle="collapse"
			data-target="#collapsibleNavbar">
			<span class="navbar-toggler-icon"></span>
		</button>

		<div class="collapse navbar-collapse" id="collapsibleNavbar">
			<ul class="navbar-nav ml-auto ">
				<li class="nav-item mr-3 mt-1">
					<form class="example" action="action_page.php">
						<input type="text" placeholder="Search.." name="search">
						<button type="submit">
							<i class="fa fa-search text-secondary"></i>
						</button>
					</form>
				</li>
				<li class="nav-item mr-3 mt-1">
					<div class="text-center">
						<i class="fa fa-envelope-o text-white" style="font-size: 25px"></i><br>
						<span class="text-white" style="font-size: 12px;">Messaging</span>
					</div>
				</li>
				<li class="nav-item mr-3 mt-1">
					<div class="text-center">
						<i class="fa fa-bell-o text-white" style="font-size: 25px"></i><br>
						<span class="text-white" style="font-size: 12px;">Notification</span>
					</div>
				</li>
				<li class="nav-item mr-3 ">
					<div class="text-center">
						<a href="#"> <img src="images/avatar.png"
							class="rounded-circle"
							style="height: 30px; width: auto; display: inline;"><br>
							<span class="text-white" style="font-size: 12px; display: block;">Me</span>
						</a>
					</div>
				</li>

			</ul>
		</div>
	</nav>
	<div class="container fluid mt-3" style="min-height: 500px;">
		<div class="row">
			<div class="col-sm-12">
				<h3>Thread List</h3>
			</div>
			<div class="col-sm-12 text-right mb-2">
				<a href="ThreadPost.html" class="btn btn-danger"
					style="width: 150px;">Post Thread <i
					class="fa fa-pencil-square-o"></i></a>
			</div>
			<!-- list -->
			<div class="card" style="width: 100%;">
				<div class="card-body">
					<div class="container">


						<div class="row">
							<div class="col-sm-2">
								<img src="images/avatar.png" style="width: 100px;">
							</div>
							<div class="col-sm-10 text-left">
								<div class="col-sm-12">
									<a href="">
										<h3>Title</h3>
									</a>
								</div>
								<div class="col-sm-12">
									<p>sdfsadfasjfhsjadhfjsahfjsahdfjashdjfahsdjfahsdjfsahjfsahjashjahfjsa
										jfdsjfshajfsad jsdhfjshf sfdj</p>
								</div>
							</div>
						</div>




					</div>
				</div>
			</div>
			<!-- end of list -->


			<!-- list -->
			<div class="card" style="width: 100%;">
				<div class="card-body">
					<div class="container">


						<div class="row">
							<div class="col-sm-2">
								<img src="images/avatar.png" style="width: 100px;">
							</div>
							<div class="col-sm-10 text-left">
								<div class="col-sm-12">
									<a href="">
										<h3>Title</h3>
									</a>
								</div>
								<div class="col-sm-12">
									<p>sdfsadfasjfhsjadhfjsahfjsahdfjashdjfahsdjfahsdjfsahjfsahjashjahfjsa
										jfdsjfshajfsad jsdhfjshf sfdj</p>
								</div>
							</div>
						</div>




					</div>
				</div>
			</div>
			<!-- end of list -->


			<!-- list -->
			<div class="card" style="width: 100%;">
				<div class="card-body">
					<div class="container">


						<div class="row">
							<div class="col-sm-2">
								<img src="images/avatar.png" style="width: 100px;">
							</div>
							<div class="col-sm-10 text-left">
								<div class="col-sm-12">
									<a href="">
										<h3>Title</h3>
									</a>
								</div>
								<div class="col-sm-12">
									<p>sdfsadfasjfhsjadhfjsahfjsahdfjashdjfahsdjfahsdjfsahjfsahjashjahfjsa
										jfdsjfshajfsad jsdhfjshf sfdj</p>
								</div>
							</div>
						</div>


					</div>
				</div>
			</div>
			<!-- end of list -->


			<!-- list -->
			<div class="card" style="width: 100%;">
				<div class="card-body">
					<div class="container">


						<div class="row">
							<div class="col-sm-2">
								<img src="images/avatar.png" style="width: 100px;">
							</div>
							<div class="col-sm-10 text-left">
								<div class="col-sm-12">
									<a href="">
										<h3>Title</h3>
									</a>
								</div>
								<div class="col-sm-12">
									<p>sdfsadfasjfhsjadhfjsahfjsahdfjashdjfahsdjfahsdjfsahjfsahjashjahfjsa
										jfdsjfshajfsad jsdhfjshf sfdj</p>
								</div>
							</div>
						</div>



					</div>
				</div>
			</div>
			<!-- end of list -->


		</div>
	</div>

	<div class="container-fluid bg-secondary text-white mt-5">
		<div class="row">
			<div class="col-sm-12 text-center p-4">
				<p>Copyright &copy; M. Fahmi Fachrozi</p>
			</div>
		</div>
	</div>

</body>
</html>