<!DOCTYPE html>
<html lang="en">
<head>
<title>ABC Jobs Portal - Welcome</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<script
	src="https://cdn.ckeditor.com/ckeditor5/23.0.0/decoupled-document/ckeditor.js"></script>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="css/style.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
<style>
.card {
	width: 25rem;
	text-align: left;
}

body {
	font-size: 14px;
}
</style>
</head>

<body>
	<nav class="navbar navbar-expand-sm bg-dark navbar-dark">
		<h2 class="navbar-brand text-white">
			<a href="profile.html" style="color: white; text-decoration: none;">
				<img src="images/logo.png" style="width: 50px; height: auto;">
				Jobs
			</a>
		</h2>

		<button class="navbar-toggler" type="button" data-toggle="collapse"
			data-target="#collapsibleNavbar">
			<span class="navbar-toggler-icon"></span>
		</button>

		<div class="collapse navbar-collapse" id="collapsibleNavbar">
			<ul class="navbar-nav ml-auto ">
				<li class="nav-item mr-3 mt-1">
					<form class="example" action="action_page.php">
						<input type="text" placeholder="Search.." name="search">
						<button type="submit">
							<i class="fa fa-search text-secondary"></i>
						</button>
					</form>
				</li>
				<li class="nav-item mr-3 mt-1">
					<div class="text-center">
						<i class="fa fa-envelope-o text-white" style="font-size: 25px"></i><br>
						<span class="text-white" style="font-size: 12px;">Messaging</span>
					</div>
				</li>
				<li class="nav-item mr-3 mt-1">
					<div class="text-center">
						<i class="fa fa-bell-o text-white" style="font-size: 25px"></i><br>
						<span class="text-white" style="font-size: 12px;">Notification</span>
					</div>
				</li>
				<li class="nav-item mr-3 ">
					<div class="text-center">
						<a href="#"> <img src="images/avatar.png"
							class="rounded-circle"
							style="height: 30px; width: auto; display: inline;"><br>
							<span class="text-white" style="font-size: 12px; display: block;">Me</span>
						</a>
					</div>
				</li>

			</ul>
		</div>
	</nav>
	<div class="container fluid mt-3" style="min-height: 500px;">
		<div class="row">
			<div class="col-sm-12">
				<h3>Post a thread</h3>
			</div>
			<!-- title -->
			<div class="col-sm-12">
				<div class="form-group mb-4">
					<input type="text" class="form-control" id="exampleInputEmail1"
						aria-describedby="emailHelp" placeholder="Title">
				</div>
			</div>
			<!-- end of title  -->
			<!-- content -->
			<div class="col-sm-12">

				<div id="toolbar-container"></div>
				<div id="editor" style="min-height: 200px; border: 1px solid grey;">
					<p>This is the initial editor content.</p>
				</div>

			</div>
			<!-- end of content -->
			<div class="col-sm-12 mt-2 text-right">
				<button class="btn btn-primary" style="width: 100px;">Post</button>
			</div>
		</div>
	</div>

	<div class="container-fluid bg-secondary text-white mt-5">
		<div class="row">
			<div class="col-sm-12 text-center p-4">
				<p>Copyright &copy; M. Fahmi Fachrozi</p>
			</div>
		</div>
	</div>


	<script>
    DecoupledEditor
        .create( document.querySelector( '#editor' ) )
        .then( editor => {
            const toolbarContainer = document.querySelector( '#toolbar-container' );

            toolbarContainer.appendChild( editor.ui.view.toolbar.element );
        } )
        .catch( error => {
            console.error( error );
        } );
</script>
</body>
</html>