<!DOCTYPE html>
<html lang="en">
<head>
<title>ABC Jobs Portal - Welcome</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
<style>
@media only screen and (max-width: 768px) {
	/* For mobile phones: */
	.regist-btn {
		margin-bottom: 10px;
		width: 100px;
	}
	.login-btn {
		width: 100px;
	}
}
</style>
</head>

<body>
	<nav class="navbar navbar-expand-sm bg-dark navbar-dark">
		<h2 class="navbar-brand text-white">
			<img src="images/logo.png" style="width: 50px; height: auto;">
			Jobs
		</h2>
		<button class="navbar-toggler" type="button" data-toggle="collapse"
			data-target="#collapsibleNavbar">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse" id="collapsibleNavbar">
			<ul class="navbar-nav ml-auto">
				<li class="nav-item"><a href="register.jsp"
					class="btn btn-success mr-1 regist-btn">Register</a></li>
				<li class="nav-item"><a href="login.jsp"
					class="btn btn-primary login-btn">Login</a></li>

			</ul>
		</div>
	</nav>
	<div class="container" style="min-height: 450px;">
		<div class="row mt-5">
			<div class="col-sm-12 col-md-8 col-lg-8">
				<img src="images/website-banner.jpg"
					style="width: 100%; height: auto;">
			</div>
			<div class="col-sm-12 col-md-4 col-lg-4">
				<h1 class="text-info">Welcome to ABC Jobs Portal</h1>

				<form>
					<div class="form-group">

						<input type="email" class="form-control" id="exampleInputEmail1"
							aria-describedby="emailHelp" placeholder="Enter email">

					</div>
					<div class="form-group">

						<input type="password" class="form-control"
							id="exampleInputPassword1" placeholder="Password">
					</div>
					<button type="submit" class="btn btn-primary" style="width: 100%;">Login</button>
				</form>
				<hr>
				<a href="reset-password.jsp">Forgot Password?</a>

			</div>
		</div>
	</div>

	<!-- footer -->
	<div class="container-fluid bg-secondary text-white mt-5">
		<div class="row">
			<div class="col-sm-12 text-center p-4">
				<p>Copyright &copy; M. Fahmi Fachrozi</p>
			</div>
		</div>
	</div>

</body>
</html>