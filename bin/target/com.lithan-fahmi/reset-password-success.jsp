<!DOCTYPE html>
<html lang="en">
<head>
<title>ABC Jobs Portal - Reset Password Success</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

</head>

<body>

	<div class="container mt-3" style="min-height: 550px;">
		<div class="row">
			<div class="col-sm-12">
				<a href="index.html" class="text-secondary"
					style="font-size: 2rem; text-decoration: none;"> <i
					class="fa fa-chevron-circle-left"></i> Index
				</a>
			</div>
		</div>
		<div class="row mt-5">
			<div class="col-sm-12 text-center">
				<h2>
					<img src="images/logo.png" style="width: 50px; height: auto;">
					Jobs Portal
				</h2>
			</div>
			<div class="col-sm-8 text-center text-success mt-5"
				style="margin: auto;">


				<div class="alert alert-success mt-2" role="alert">
					<i class="fa fa-check-circle" aria-hidden="true"
						style="font-size: 5rem;"></i>
					<h4 class="">Reset Password Success!</h4>
					<p>Please check your email for your new password.</p>
				</div>

			</div>
		</div>
	</div>

	<!-- footer -->
	<div class="container-fluid bg-secondary text-white mt-5">
		<div class="row">
			<div class="col-sm-12 text-center p-4">
				<p>Copyright &copy; M. Fahmi Fachrozi</p>
			</div>
		</div>
	</div>
</body>
</html>