package com.action;

import java.util.Map;

import com.models.JobModel;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class CommentAction extends ActionSupport {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String comment;
	private String jobId;
	private Map session = (Map) ActionContext.getContext().get("session");
	private JobModel jobModel = null;
	
	private String url;
	


	public String execute() throws Exception{
		try {
			
			String isLoggedIn = (String) session.get("username");
			
			if(isLoggedIn.length() > 0) {
				String account_id = (String) session.get("user_id");
				jobModel = new JobModel();
				int insert = jobModel.insertComment(jobId,comment, account_id);
				
				url="detail-job?q="+jobId;
				if(insert > 0 ) {
					
					return "success";
				}
				return "success";
			}
			
			return "success";
		}catch(Exception e) {
			e.printStackTrace();
			return "success";
		}
	}
	
	
	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public String getJobId() {
		return jobId;
	}

	public void setJobId(String jobId) {
		this.jobId = jobId;
	}


	public String getUrl() {
		return url;
	}


	public void setUrl(String url) {
		this.url = url;
	}
	

}
