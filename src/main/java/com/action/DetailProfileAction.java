package com.action;

import java.sql.Blob;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

import com.bean.User;
import com.bean.Job;
import com.bean.Thread;
import com.models.JobModel;
import com.models.ThreadModel;
import com.models.UserModel;
import com.opensymphony.xwork2.ActionSupport;

public class DetailProfileAction extends ActionSupport{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String username;
	private ResultSet rs = null;
	private User user = null;
	private UserModel userModel = null;
	private User userBean;
	//thread
	private Thread thread;
	private List<Thread> threadList = null;
	private ThreadModel threadModel = null;
	
	//thread recent
	private List<Thread> threadRecentList = null;
	//job recent
	private Job job;
	private List<Job> jobRecentList = null;
	private JobModel jobModel = null;
	
	public String execute() throws Exception{ 
		
		
			try {
				if(username != null) {
					userModel = new UserModel();
					rs = userModel.getAllUserData(username);
					
					int i = 0;
					String user_id = null;
					
					if(rs != null) {
						while(rs.next()) {
							i++;
							user = new User();
							user_id = rs.getString("account_id");
							user.setUsername(rs.getString("username"));
							user.setFirstName(rs.getString("first_name"));
							user.setLastName(rs.getString("last_name"));
							user.setEmail(rs.getString("email"));
							user.setCountry(rs.getString("c_id"));
						
							user.setCountryName(rs.getString("country_name"));
							user.setUser_id(rs.getString("account_id"));
							
							if(rs.getBlob("profile_picture") != null) { 
							//get user image and convert to base64
							Blob c = rs.getBlob("profile_picture");
							byte[] data = c.getBytes(1, (int) c.length());

							String base64 = "data:image/jpeg;base64,"+Base64.getEncoder().encodeToString(data);
							
							user.setProfilePicture(base64);
							}
							
						}
					}
					
					//getting user's thread history
					threadModel = new ThreadModel();
					rs = threadModel.getUserThread(user_id);
					if(rs != null) {
						
						threadList = new ArrayList<Thread>();
						while(rs.next()) {
							
							thread = new Thread();
							thread.setThreadId(rs.getString("thread_id"));
							thread.setTitle(rs.getString("title"));
							thread.setContent(rs.getString("content"));
							thread.setPost_date(rs.getString("post_date"));
							
							
							threadList.add(thread);
						}
					}
					
					
					//getting recent thread
					rs = threadModel.getRecentThread();
					
					if(rs != null) {
						threadRecentList = new ArrayList<Thread>();
						while(rs.next()) {
							
							
							thread = new Thread();
							thread.setThreadId(rs.getString("thread_id"));
							thread.setTitle(rs.getString("title"));
							thread.setContent(rs.getString("content"));
							thread.setPost_date(rs.getString("post_date"));
							
							
							userBean = new User();
							userBean.setUsername(rs.getString("username"));
							userBean.setFirstName(rs.getString("first_name"));
							userBean.setLastName(rs.getString("last_name"));
							
						if(rs.getBlob("profile_picture") != null) { 
							//get user image and convert to base64
							Blob c = rs.getBlob("profile_picture");
							byte[] data = c.getBytes(1, (int) c.length());

							String base64 = "data:image/jpeg;base64,"+Base64.getEncoder().encodeToString(data);
							
							userBean.setProfilePicture(base64);
						}
							
							thread.setUser(userBean);
							threadRecentList.add(thread);
							
						}
					}
					//getting recent jobs
					
					jobModel = new JobModel();
					rs = jobModel.getRecentJob();
				
					
					if(rs != null) {
						jobRecentList = new ArrayList<Job>();
						while(rs.next()) {
							
							
							job = new Job();
							job.setAccountId(rs.getString("account_id"));
							job.setJobId(rs.getString("job_id"));
							job.setJobTitle(rs.getString("job_title"));
							job.setCategory(rs.getString("category"));
							job.setDescription(rs.getString("description"));
							job.setStart_date(rs.getString("start_date"));
							job.setEnd_date(rs.getString("end_date"));
							job.setPost_date(rs.getString("post_date"));
							
							userBean = new User();
							userBean.setUsername(rs.getString("username"));
							userBean.setFirstName(rs.getString("first_name"));
							userBean.setLastName(rs.getString("last_name"));
							
					if(rs.getBlob("profile_picture") != null) { 	
							//get user image and convert to base64
							Blob c = rs.getBlob("profile_picture");
							byte[] data = c.getBytes(1, (int) c.length());

							String base64 = "data:image/jpeg;base64,"+Base64.getEncoder().encodeToString(data);
							
							userBean.setProfilePicture(base64);
							
					}	
							job.setUser(userBean);
							jobRecentList.add(job);
							
						}
					}
					
					
					if(i > 0) {
						return "success";
					}
					
					return "failed";
				}
				
				return "failed";
			}catch(Exception e) {
					e.printStackTrace();
					return "failed";
					
			}
			
	}
	
	public Thread getThread() {
		return thread;
	}

	public void setThread(Thread thread) {
		this.thread = thread;
	}

	public List<Thread> getThreadList() {
		return threadList;
	}

	public void setThreadList(List<Thread> threadList) {
		this.threadList = threadList;
	}

	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}



	public List<Thread> getThreadRecentList() {
		return threadRecentList;
	}

	public void setThreadRecentList(List<Thread> threadRecentList) {
		this.threadRecentList = threadRecentList;
	}

	public List<Job> getJobRecentList() {
		return jobRecentList;
	}

	public void setJobRecentList(List<Job> jobRecentList) {
		this.jobRecentList = jobRecentList;
	}

	public User getUserBean() {
		return userBean;
	}

	public void setUserBean(User userBean) {
		this.userBean = userBean;
	}


}
