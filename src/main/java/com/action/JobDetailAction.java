package com.action;

import java.sql.Blob;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

import com.bean.CommentJob;
import com.bean.Job;
import com.bean.User;
import com.models.JobModel;
import com.opensymphony.xwork2.ActionSupport;

public class JobDetailAction extends ActionSupport {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String q;
	
	private Job job;
	private CommentJob commentJob;
	private List<CommentJob> commentJobList = null;
	private User user;
	ResultSet rs = null;
	private JobModel jobModel;
	private User userBean;
	
	
	
	public String execute() throws Exception{ 
		
		try {
			
			if(q != null) {
				
				jobModel = new JobModel();
				
				rs = jobModel.getDetailJob(q);
				
				int i = 0;
				if(rs != null){
					
					while(rs.next()) {
						i++;
						
						job = new Job();
						job.setAccountId(rs.getString("account_id"));
						job.setJobId(rs.getString("job_id"));
						job.setJobTitle(rs.getString("job_title"));
						job.setCategory(rs.getString("category"));
						job.setDescription(rs.getString("description"));
						job.setStart_date(rs.getString("start_date"));
						job.setEnd_date(rs.getString("end_date"));
						job.setPost_date(rs.getString("post_date"));
						
						user = new User();
						user.setUsername(rs.getString("username"));
						user.setFirstName(rs.getString("first_name"));
						user.setLastName(rs.getString("last_name"));
						
					if(rs.getBlob("profile_picture") != null) { 
						//get user image and convert to base64
						Blob c = rs.getBlob("profile_picture");
						byte[] data = c.getBytes(1, (int) c.length());

						String base64 = "data:image/jpeg;base64,"+Base64.getEncoder().encodeToString(data);
						
						user.setProfilePicture(base64);
					}
						job.setUser(user);
						
						
						
					}
				}
				
				
				//getting job's comment data
				rs = jobModel.getCommentJob(q);
				
				if(rs != null){
					commentJobList = new ArrayList<CommentJob>();
					while(rs.next()) {
						i++;
						
						commentJob = new CommentJob();
						commentJob.setJob_commet_id(rs.getString("job_comment_id"));
						commentJob.setJob_id(rs.getString("job_id"));
						commentJob.setAccount_id(rs.getString("account_id"));
						commentJob.setResponse(rs.getString("response"));
						commentJob.setResponse_date(rs.getString("response_date"));
						
						userBean = new User();
						userBean.setUsername(rs.getString("username"));
						userBean.setFirstName(rs.getString("first_name"));
						userBean.setLastName(rs.getString("last_name"));
						
					if(rs.getBlob("profile_picture") != null) { 
						//get user image and convert to base64
						Blob c = rs.getBlob("profile_picture");
						byte[] data = c.getBytes(1, (int) c.length());

						String base64 = "data:image/jpeg;base64,"+Base64.getEncoder().encodeToString(data);
						
						userBean.setProfilePicture(base64);
					}
					
						commentJob.setUser(userBean);
						commentJobList.add(commentJob);
						
						
						
					}
				}
				
				if(i > 0 ) {
					
					return "success";
				}else {
					return "failed";
				}
			}
			
			return "notfound";
		}catch(Exception e) {
			e.printStackTrace();
			return "failed";
		}
		
		
	}



	public Job getJob() {
		return job;
	}



	public void setJob(Job job) {
		this.job = job;
	}
	
	public String getQ() {
		return q;
	}



	public void setQ(String q) {
		this.q = q;
	}



	public User getUser() {
		return user;
	}



	public void setUser(User user) {
		this.user = user;
	}



	public List<CommentJob> getCommentJobList() {
		return commentJobList;
	}



	public void setCommentJobList(List<CommentJob> commentJobList) {
		this.commentJobList = commentJobList;
	}

	
	
	
}
