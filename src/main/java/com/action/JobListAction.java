package com.action;

import java.sql.Blob;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

import com.bean.Job;
import com.bean.User;
import com.models.JobModel;
import com.opensymphony.xwork2.ActionSupport;

public class JobListAction extends ActionSupport{
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Job job;
	private List<Job> jobList = null;
	private JobModel jobModel = null;
	ResultSet rs = null;
	private User user;
	
	

	//Get job lisst data
	public String execute() throws Exception{
			
			
			try {
				int i = 0;
				jobModel = new JobModel();
				rs = jobModel.getAllJob();
			
				
				if(rs != null) {
					jobList = new ArrayList<Job>();
					while(rs.next()) {
						i++;
						
						job = new Job();
						job.setAccountId(rs.getString("account_id"));
						job.setJobId(rs.getString("job_id"));
						job.setJobTitle(rs.getString("job_title"));
						job.setCategory(rs.getString("category"));
						job.setDescription(rs.getString("description"));
						job.setStart_date(rs.getString("start_date"));
						job.setEnd_date(rs.getString("end_date"));
						job.setPost_date(rs.getString("post_date"));
						
						user = new User();
						user.setUsername(rs.getString("username"));
						user.setFirstName(rs.getString("first_name"));
						user.setLastName(rs.getString("last_name"));
						
					if(rs.getBlob("profile_picture") != null) { 
						//get user image and convert to base64
						Blob c = rs.getBlob("profile_picture");
						byte[] data = c.getBytes(1, (int) c.length());

						String base64 = "data:image/jpeg;base64,"+Base64.getEncoder().encodeToString(data);
					
						user.setProfilePicture(base64);
					}	
						
						job.setUser(user);
						jobList.add(job);
						
					}
				}
				
				if(i > 0 ) {
					
					return "success";
				}
				
				return "success";
				
			}catch(Exception e) {
				e.printStackTrace();
				return "failed";
			}
			
		}


	public Job getJob() {
		return job;
	}


	public void setJob(Job job) {
		this.job = job;
	}


	public List<Job> getJobList() {
		return jobList;
	}


	public void setJobList(List<Job> jobList) {
		this.jobList = jobList;
	}


	public User getUser() {
		return user;
	}


	public void setUser(User user) {
		this.user = user;
	}


}
