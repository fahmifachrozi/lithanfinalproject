package com.action;

import java.sql.Blob;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.Map;

import com.bean.User;
import com.models.UserModel;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class ListUserAction extends ActionSupport {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	


	private Map session = (Map) ActionContext.getContext().get("session");
	private User user;
	private List<User> listUser = null;
	private UserModel userModel = null;
	ResultSet rs = null;
	private String msg;
	
	public String execute() throws Exception{
		
	
		String user_role = (String) session.get("role_id");
		try {
			String adm = "2";
			if(user_role.equals(adm)) {
				
			
			
				userModel = new UserModel();
				rs = userModel.getAllUser();
				int i = 0;
				if(rs != null) {
					listUser = new ArrayList<User>();
					while(rs.next()) {
						i++;
						user = new User();
						
						user.setUser_id(rs.getString("account_id"));
						user.setRole_id(rs.getString("role_id"));
						user.setUsername(rs.getString("username"));
						user.setFirstName(rs.getString("first_name"));
						user.setLastName(rs.getString("last_name"));
						user.setEmail(rs.getString("email"));
						
					if(rs.getBlob("profile_picture") != null) { 	
						//get user image and convert to base64
						Blob c = rs.getBlob("profile_picture");
						byte[] data = c.getBytes(1, (int) c.length());

						String base64 = "data:image/jpeg;base64,"+Base64.getEncoder().encodeToString(data);
						
						user.setProfilePicture(base64);
						
					}
						user.setSuspended(rs.getString("suspended"));
						
						listUser.add(user);
						
				
						
					}
				}
				
				
				if(i > 0) {
					
					return "success";
				}
				
				
			}
				return "failed";
		}catch(Exception e){
			e.printStackTrace();
			return "failed";
		}
		
	}
	
	
	public List<User> getListUser() {
		return listUser;
	}


	public void setListUser(List<User> listUser) {
		this.listUser = listUser;
	}


	public String getMsg() {
		return msg;
	}


	public void setMsg(String msg) {
		this.msg = msg;
	}
	

}
