package com.action;

import java.nio.charset.StandardCharsets;
import java.sql.Blob;
import java.sql.ResultSet;
import java.util.Base64;
import java.util.Map;

import org.apache.struts2.dispatcher.SessionMap;
import org.apache.struts2.interceptor.SessionAware;

import com.bean.User;
import com.google.common.hash.Hashing;
import com.helpers.ExistenceChecker;
import com.models.UserModel;
import com.opensymphony.xwork2.ActionSupport;

public class LoginAction extends ActionSupport implements SessionAware{
	/**
	 * 
	 */
	
	private static final long serialVersionUID = 1L;
	private String usernameOrEmail = "";
	private String password = "";
	private ResultSet rs = null;
	private User user = null;
	private int countRow = 0;
	private String url;
	private String userCountry = "";
	
	

	private SessionMap<String, Object> userSession;
	
	private UserModel userModel = new UserModel();
	private String errorMsg;
	


	
	public String execute() throws Exception{
		
		
		String hashPassword = Hashing.sha256().hashString(password, StandardCharsets.UTF_8).toString();
		rs = userModel.getUser(usernameOrEmail, hashPassword);
		
		try {
			
			//check user's suspended status
			ExistenceChecker check = new ExistenceChecker();
			if(check.isSuspended(usernameOrEmail)) {
				errorMsg="Your account has been suspended!";
				return "failed";
			}
			
			if(rs != null) {
				while(rs.next()) {
					countRow++;
					
					
					user = new User();
					
					if(rs.getBlob("profile_picture") != null) { 
						//get user image and convert to base64
						Blob c = rs.getBlob("profile_picture");
						byte[] data = c.getBytes(1, (int) c.length());

						String base64 = "data:image/jpeg;base64,"+Base64.getEncoder().encodeToString(data);
						
						user.setProfilePicture(base64);
						userSession.put("profile_picture", base64);
					}
					
					userSession.put("isLoggedIn", true);
					userSession.put("username", rs.getString("username"));
					
					userSession.put("user_id", rs.getString("account_id"));
					userSession.put("role_id", rs.getString("role_id"));
					userSession.put("username", rs.getString("username"));
					url = "profile?username="+rs.getString("username");			
				
				
				}
			}
			
			if(countRow > 0) {
				
				return "success";
			}
			
			errorMsg = "Username or Password is wrong!";
			return "failed";
			
		}catch(Exception e) {
			
			e.printStackTrace();
			
			return "failed";
		}
		
		
	}
	
	public String logout(){  
	    if(userSession!=null){  
	    	userSession.invalidate();  
	    }  
	    return "logout";  
	}
	
	public void validate() {
		
		

		if(password.length() == 0 ) {
			
			errorMsg="password is required!";
			addFieldError("password", "Password is required.");
		}
		
		
		if(usernameOrEmail.length() == 0) {
			
			errorMsg="Username Or Email is required!";
			addFieldError("usernameOrEmail", "Username is required.");
			
		}
		

		
			
	}
	
	
	public void setSession(Map<String, Object> session) {
		// TODO Auto-generated method stub
		this.userSession = (SessionMap)session;
	}
	
	public String getErrorMsg() {
		return errorMsg;
	}
	
	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getUserCountry() {
		return userCountry;
	}

	public void setUserCountry(String userCountry) {
		this.userCountry = userCountry;
	}
	
	public String getUsernameOrEmail() {
		return usernameOrEmail;
	}
	public void setUsernameOrEmail(String usernameOrEmail) {
		this.usernameOrEmail = usernameOrEmail;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
	

	
	
}
