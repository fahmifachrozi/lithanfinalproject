package com.action;

import org.apache.struts2.dispatcher.SessionMap;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class LogoutAction extends ActionSupport{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	SessionMap session = (SessionMap) ActionContext.getContext().getSession();
	
	public String execute() throws Exception{
		
		try {
			
			//invalidate
			session.invalidate();
			
			return "success";
		}catch(Exception e){
			e.printStackTrace();
			
			return "failed";
		}
	}	
}
