package com.action;

import java.sql.Blob;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.Map;

import com.bean.MessageHistory;
import com.bean.User;
import com.models.MessageModel;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class MessageHistoryAction extends ActionSupport{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String friendId;
	private MessageHistory messageHistory;
	private List<MessageHistory> listMessageHistory = null;
	private MessageModel messageModel;
	private Map session = (Map) ActionContext.getContext().get("session");
	ResultSet rs;
	private User user;
	
	public String execute() throws Exception{
		
		
		try {
			
			String isLoggedIn = (String) session.get("username");
			String myId = (String) session.get("user_id");
			messageModel = new MessageModel();
			
			if(isLoggedIn.length() > 0) {
			if(friendId.equals(myId)) {
				return "failed";
			}	
			rs = messageModel.getMessageHistory(friendId, myId);
			
			if(rs != null) {
				listMessageHistory = new ArrayList<MessageHistory>();
				while(rs.next()) {
					messageHistory = new MessageHistory();
					messageHistory.setMessage_id(rs.getString("m_id"));
					messageHistory.setSender_id(rs.getString("sender_id"));
					messageHistory.setReceiver_id(rs.getString("receiver_id"));
					messageHistory.setMessage(rs.getString("message"));
					messageHistory.setTime(rs.getString("time"));
					messageHistory.setIs_read(rs.getString("is_read"));
					messageHistory.setRead_time(rs.getString("read_time"));
					
					user = new User();
					user.setUser_id(rs.getString("account_id"));
					user.setUsername(rs.getString("username"));
					user.setFirstName(rs.getString("first_name"));
					user.setLastName(rs.getString("last_name"));
					if(rs.getBlob("profile_picture") != null) { 
						//get user image and convert to base64
						Blob c = rs.getBlob("profile_picture");
						byte[] data = c.getBytes(1, (int) c.length());

						String base64 = "data:image/jpeg;base64,"+Base64.getEncoder().encodeToString(data);
						
						user.setProfilePicture(base64);
						}
					messageHistory.setUser(user);
					
					listMessageHistory.add(messageHistory);
				}
			}
			
			
			return "success";
			
			//if not login yet
			}else {
				return "failed";
			}
		}catch(Exception e) {
			e.printStackTrace();
			return "failed";
		}
	
		
		
	}

	
	
	public List<MessageHistory> getListMessageHistory() {
		return listMessageHistory;
	}
	public void setListMessageHistory(List<MessageHistory> listMessageHistory) {
		this.listMessageHistory = listMessageHistory;
	}



	public String getFriendId() {
		return friendId;
	}



	public void setFriendId(String friendId) {
		this.friendId = friendId;
	}
	
	

}
