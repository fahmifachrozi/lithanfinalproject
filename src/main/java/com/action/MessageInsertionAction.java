package com.action;

import java.util.Map;

import com.models.MessageModel;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class MessageInsertionAction extends ActionSupport{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Map session = (Map) ActionContext.getContext().get("session");
	private MessageModel messageModel = null;
	private String message;
	private String friendId;
	private String url;
	
	public String execute() throws Exception{
		
		
		try {
			String isLoggedIn = (String) session.get("username");
			String myId = (String) session.get("user_id");
			
			if(isLoggedIn.length() > 0) {
				
				messageModel = new MessageModel();
			
				int insert = messageModel.insertMessage(message, friendId, myId);
				
				if(insert > 0) {
					url = "message-history?friendId="+friendId;
					
					return "success";
				}
				url = "message-history?friendId="+friendId;
			}
			
			return "failed";
			
		}catch(Exception e) {
			e.printStackTrace();
			return "failed";
		}
		
	}
	
	
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getFriendId() {
		return friendId;
	}
	public void setFriendId(String friendId) {
		this.friendId = friendId;
	}


	public String getUrl() {
		return url;
	}


	public void setUrl(String url) {
		this.url = url;
	}

}
