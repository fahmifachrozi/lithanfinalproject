package com.action;

import java.sql.Blob;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.Map;

import com.bean.MessageHistory;
import com.bean.User;
import com.models.MessageModel;
import com.models.UserModel;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class MessageListAction extends ActionSupport{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Map session = (Map) ActionContext.getContext().get("session");
	private MessageModel messageModel = null;
	private MessageHistory messageHistory;
	private List<MessageHistory> listMessageHistory = null;
	ResultSet rs;
	ResultSet rs2;
	private UserModel userModel = null;
	private User user;
	
public String execute() throws Exception{
		
		
		try {
			
			String isLoggedIn = (String) session.get("username");
			String myId = (String) session.get("user_id");
			messageModel = new MessageModel();
			userModel = new UserModel();
			if(isLoggedIn.length() > 0) {
		
			rs = messageModel.getMessageList(myId);
			int i =0;
			if(rs != null) {
				i++;
				listMessageHistory = new ArrayList<MessageHistory>();
				while(rs.next()) {
					messageHistory = new MessageHistory();
					messageHistory.setMessage_id(rs.getString("myid"));
					messageHistory.setSender_id(rs.getString("sender_id"));
					messageHistory.setReceiver_id(rs.getString("receiver_id"));
					messageHistory.setMessage(rs.getString("message"));
					messageHistory.setTime(rs.getString("time"));
					messageHistory.setIs_read(rs.getString("is_read"));
					messageHistory.setRead_time(rs.getString("read_time"));
				
					//getting friend's data
					if(myId.equals(rs.getString("sender_id"))) {
						rs2 = userModel.getUserById(rs.getString("receiver_id"));
					}else {
						rs2 = userModel.getUserById(rs.getString("sender_id"));
					}
					
					if(rs2 != null) {
						
						while(rs2.next()){
							user = new User();
							user.setUser_id(rs2.getString("account_id"));
							user.setUsername(rs2.getString("username"));
							user.setFirstName(rs2.getString("first_name"));
							user.setLastName(rs2.getString("last_name"));
							
							if(rs2.getBlob("profile_picture") != null) { 
								//get user image and convert to base64
								Blob c = rs2.getBlob("profile_picture");
								byte[] data = c.getBytes(1, (int) c.length());

								String base64 = "data:image/jpeg;base64,"+Base64.getEncoder().encodeToString(data);
								
								user.setProfilePicture(base64);
								}
							messageHistory.setUser(user);
						}
						
					}
	
					
					listMessageHistory.add(messageHistory);
				}
			}
			
			if(i > 0) {
				
				return "success";
			}
			
			return "failed";
			
			//if not login yet
			}else {
				return "failed";
			}
		}catch(Exception e) {
			e.printStackTrace();
			return "failed";
		}
	
		
		
	}

public List<MessageHistory> getListMessageHistory() {
	return listMessageHistory;
}

public void setListMessageHistory(List<MessageHistory> listMessageHistory) {
	this.listMessageHistory = listMessageHistory;
}

}
