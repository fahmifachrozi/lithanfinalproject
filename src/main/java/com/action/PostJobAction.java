package com.action;

import java.util.Map;

import com.bean.Job;
import com.models.JobModel;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class PostJobAction extends ActionSupport{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Map session = (Map) ActionContext.getContext().get("session");
	private Job job;
	private JobModel jobModel;
	private String url;
	
	//Inserting job post to the database
	//login is needed
	public String execute() throws Exception{
		try {
			
			String isLoggedIn = (String) session.get("username");
			String accountId = (String) session.get("user_id");
			
			if(isLoggedIn.length() > 0) {
				jobModel = new JobModel();
				job.setAccountId(accountId);
				
				int insert = jobModel.insertJob(job);
				
				if(insert > 0) {
					setUrl("job-post?msg=true");
					return "success";
				}
				
				return "success";
			}
			
			return "failed";
		}catch(Exception e) {
			e.printStackTrace();
			return "failed";
		}
		
	}

	public Job getJob() {
		return job;
	}

	public void setJob(Job job) {
		this.job = job;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
	

}
