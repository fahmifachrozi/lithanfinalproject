package com.action;

import java.util.Map;

import com.models.JobModel;
import com.models.ThreadModel;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.bean.Thread;

public class PostThreadAction extends ActionSupport {
	
	private static final long serialVersionUID = 1L;
	private Map session = (Map) ActionContext.getContext().get("session");
	private Thread thread;
	private ThreadModel threadModel;
	private String url;
	
	
	//Inserting thread to the database
		//login is needed
		public String execute() throws Exception{
			try {
				
				String isLoggedIn = (String) session.get("username");
				String accountId = (String) session.get("user_id");
				
				if(isLoggedIn.length() > 0) {
					threadModel = new ThreadModel();
					thread.setAccountId(accountId);
					
					
					int insert = threadModel.insertThread(thread);
					
					if(insert > 0) {
						url = "thread-post?msg=true";
						return "success";
					}
					
					return "failed";
				}
				
				return "failed";
			}catch(Exception e) {
				e.printStackTrace();
				return "failed";
			}
			
		}
		
		
	public Thread getThread() {
		return thread;
	}
	public void setThread(Thread thread) {
		this.thread = thread;
	}


	public String getUrl() {
		return url;
	}


	public void setUrl(String url) {
		this.url = url;
	}
	
}
