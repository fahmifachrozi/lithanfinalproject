package com.action;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;

import com.bean.User;
import com.helpers.ExistenceChecker;
import com.models.RegistrationModel;
import com.opensymphony.xwork2.ActionSupport;
import com.google.common.hash.Hashing;

public class RegistrationAction extends ActionSupport{
		/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
		private User user;
		private RegistrationModel registration = null;
		private ArrayList<String> errorMsg = new ArrayList<String>();
		

		public User getUser() {
			return user;
		}

		public void setUser(User user) {
			this.user = user;
		}
		
		public ArrayList<String> getErrorMsg() {
			return errorMsg;
		}

		public void setErrorMsg(ArrayList<String> errorMsg) {
			this.errorMsg = errorMsg;
		}
		
		//Insertion action
		public String execute() throws Exception{
			registration = new RegistrationModel();
			
			
			try {
				String hashPassword = Hashing.sha256().hashString(user.getPassword(), StandardCharsets.UTF_8).toString();
				user.setPassword(hashPassword);
				int insert = registration.register(user);
				
				if(insert > 0) {
					return "success";
				}
				
				return "success";
			}catch(Exception e) {
				
				e.printStackTrace();
				
				return "failed";
			}
			
			
		}
		
		
		
	

		public void validate() {
			
			if(user != null) {
				ExistenceChecker check = new ExistenceChecker();
			
			if(user.getUsername().length() == 0) {
	
				errorMsg.add("Username is required!");
				addFieldError("user.username", "Username is required.");
				
			}
				
			if(check.usernameExists(user.getUsername())) {
				errorMsg.add("Username is already used!");
				addFieldError("user.username","Username already used!");
			}
			
			if(user.getFirstName().length() == 0) {
				
				errorMsg.add("Frist name is required!");
				addFieldError("user.firstName", "First name is required.");
			}
			
			if(user.getLastName().length() == 0) {
				errorMsg.add("Last name is required!");
				addFieldError("user.lastName", "Last name is required.");
			}
			
			if(user.getEmail().length() == 0) {
				errorMsg.add("Email is required!");
				addFieldError("user.email", "Email is required.");
			}
			
			if(check.emailExists(user.getEmail())) {
				errorMsg.add("Email is already used!");
				addFieldError("user.email","Email already used!");
			}
			
			if(user.getPassword().length() == 0 || user.getPassword().length() < 6){
				addFieldError("user.password", "Password is required and must at least 6 characters!");
				errorMsg.add("Password is required and must at least 6 characters!");
			}
			
		
			
			}
		}

	

		
}
