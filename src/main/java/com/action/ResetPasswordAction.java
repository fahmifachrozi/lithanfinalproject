package com.action;

import java.nio.charset.StandardCharsets;
import java.sql.ResultSet;
import java.util.Random;
import java.util.UUID;

import com.google.common.hash.Hashing;
import com.helpers.ExistenceChecker;
import com.helpers.ResetPasswordMail;
import com.models.UserModel;
import com.opensymphony.xwork2.ActionSupport;

public class ResetPasswordAction extends ActionSupport {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String email;
	private UserModel userModel;
	
	
	
	public String execute() throws Exception{
		
		try {
			
			
			
			ExistenceChecker check = new ExistenceChecker();
			
			if(check.emailExists(email) == false) {
				return "failed";
			}
			
			
			Random rnd = new Random();
		    int number = rnd.nextInt(999999);
			String newPassword = String.format("%06d", number);
		
			String hashPassword = Hashing.sha256().hashString(newPassword , StandardCharsets.UTF_8).toString();
			
			userModel = new UserModel();
			
			
			ResetPasswordMail reset = new ResetPasswordMail();
			boolean isResetSuccess = reset.send_email(email, newPassword);
			if(!isResetSuccess) {
				return "failed";
			}
			int resetPassword = userModel.resetPassword(email, hashPassword);
			
			if(resetPassword > 0) {
				return "success";
			}
			
			
			return "failed";
		}catch(Exception e) {
			e.printStackTrace();
			return "failed";
		}
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}
	
	

}
