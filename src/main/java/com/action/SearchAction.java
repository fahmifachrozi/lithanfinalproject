package com.action;

import java.sql.Blob;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

import com.bean.Country;
import com.bean.User;
import com.models.UserModel;
import com.opensymphony.xwork2.ActionSupport;

public class SearchAction extends ActionSupport{
	private String query;
	private ResultSet rs;
	private User user = null;
	private List<User> userList = null;
	private UserModel userModel = null;
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	public String execute() throws Exception{ 
		
		try {
			
			if(query != null) {
				
				userModel = new UserModel();
				rs = userModel.searchUser(query);
				
				int i = 0;
				if(rs != null){
					userList = new ArrayList<User>();
					while(rs.next()) {
						i++;
						user = new User();
						
						user.setUsername(rs.getString("username"));
						user.setFirstName(rs.getString("first_name"));
						user.setLastName(rs.getString("last_name"));
						user.setEmail(rs.getString("email"));
						user.setCountry(rs.getString("c_id"));
						
						user.setCountryName(rs.getString("country_name"));
						user.setUser_id(rs.getString("account_id"));
						
						
						if(rs.getBlob("profile_picture") != null) { 
						//get user image and convert to base64
						Blob c = rs.getBlob("profile_picture");
						byte[] data = c.getBytes(1, (int) c.length());

						String base64 = "data:image/jpeg;base64,"+Base64.getEncoder().encodeToString(data);
					
						user.setProfilePicture(base64);
						
						}
						
						userList.add(user);
						
					}
				}
				
				
				if(i > 0 ) {
					
					return "success";
				}else {
					return "success";
				}
			}
			
			return "notfound";
		}catch(Exception e) {
			e.printStackTrace();
			return "failed";
		}
		
		
	}


	public String getQuery() {
		return query;
	}


	public void setQuery(String query) {
		this.query = query;
	}


	public User getUser() {
		return user;
	}


	public void setUser(User user) {
		this.user = user;
	}


	public List<User> getUserList() {
		return userList;
	}


	public void setUserList(List<User> userList) {
		this.userList = userList;
	}



}
