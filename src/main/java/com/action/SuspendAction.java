package com.action;

import java.util.Map;

import com.models.UserModel;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class SuspendAction extends ActionSupport{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String username;
	private UserModel userModel;
	private Map session = (Map) ActionContext.getContext().get("session");
	
	
	public String execute() throws Exception{
		
		try {
			String user_role = (String) session.get("role_id");
			if(user_role.equals("2")) {
				
				userModel = new UserModel();
				int suspend = userModel.suspendUser(username);
				
				if(suspend > 0) {
					
					return "success";
				}
			}
			
			
			return "failed";
		}catch(Exception e) {
			e.printStackTrace();
			return "failed";
		}
	}


	public String getUsername() {
		return username;
	}


	public void setUsername(String username) {
		this.username = username;
	}

}
