package com.action;

import java.util.Map;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class ThreadAction extends ActionSupport{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Map session = (Map) ActionContext.getContext().get("session");
	private String msg;
	
	//accessing job posting page
	//login is needed
	public String execute() throws Exception{
		try {
			
			String isLoggedIn = (String) session.get("username");
			
			if(isLoggedIn.length() > 0) {
				
				
				return "success";
				
			}
			
			return "failed";
		}catch(Exception e) {
			e.printStackTrace();
			return "failed";
		}
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

}
