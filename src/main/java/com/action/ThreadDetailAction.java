package com.action;

import com.opensymphony.xwork2.ActionSupport;

import java.sql.Blob;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Base64;

import com.bean.Thread;
import com.bean.User;
import com.models.ThreadModel;
import com.models.UserModel;

public class ThreadDetailAction extends ActionSupport{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String q;
	private Thread thread;
	private User user;
	ResultSet rs = null;
	private ThreadModel threadModel;
	
	
	
	public String execute() throws Exception{ 
		
		try {
			
			if(q != null) {
				
				threadModel = new ThreadModel();
				
				rs = threadModel.getDetailThread(q);
				
				int i = 0;
				if(rs != null){
					
					while(rs.next()) {
						i++;
						
						thread = new Thread();
						thread.setThreadId(rs.getString("thread_id"));
						thread.setTitle(rs.getString("title"));
						thread.setContent(rs.getString("content"));
						thread.setPost_date(rs.getString("post_date"));
						
						user = new User();
						user.setUsername(rs.getString("username"));
						user.setFirstName(rs.getString("first_name"));
						user.setLastName(rs.getString("last_name"));
						
						if(rs.getBlob("profile_picture") != null) { 
							//get user image and convert to base64
							Blob c = rs.getBlob("profile_picture");
							byte[] data = c.getBytes(1, (int) c.length());

							String base64 = "data:image/jpeg;base64,"+Base64.getEncoder().encodeToString(data);
						
							user.setProfilePicture(base64);
							
							}
						
						thread.setUser(user);
						
						
						
					}
				}
				
				
				if(i > 0 ) {
					
					return "success";
				}else {
					return "failed";
				}
			}
			
			return "notfound";
		}catch(Exception e) {
			e.printStackTrace();
			return "failed";
		}
		
		
	}
	
	public String getQ() {
		return q;
	}
	public void setQ(String q) {
		this.q = q;
	}
	public Thread getThread() {
		return thread;
	}
	public void setThread(Thread thread) {
		this.thread = thread;
	}
	

}
