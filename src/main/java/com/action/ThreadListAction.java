package com.action;

import java.sql.Blob;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

import com.bean.Job;
import com.bean.User;
import com.models.JobModel;
import com.models.ThreadModel;
import com.opensymphony.xwork2.ActionSupport;
import com.bean.Thread;

public class ThreadListAction extends ActionSupport{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Thread thread;
	private List<Thread> threadList = null;
	ResultSet rs = null;
	private User user;
	private ThreadModel threadModel = null;
	
	//Get job lisst data
	public String execute() throws Exception{
			
			
			try {
				int i = 0;
				threadModel = new ThreadModel();
				rs = threadModel.getAllThread();
			
				
				if(rs != null) {
					threadList = new ArrayList<Thread>();
					while(rs.next()) {
						i++;
						
						thread = new Thread();
						thread.setThreadId(rs.getString("thread_id"));
						thread.setTitle(rs.getString("title"));
						thread.setContent(rs.getString("content"));
						thread.setPost_date(rs.getString("post_date"));
						
						
						user = new User();
						user.setUsername(rs.getString("username"));
						user.setFirstName(rs.getString("first_name"));
						user.setLastName(rs.getString("last_name"));
						if(rs.getBlob("profile_picture") != null) { 
							//get user image and convert to base64
							Blob c = rs.getBlob("profile_picture");
							byte[] data = c.getBytes(1, (int) c.length());

							String base64 = "data:image/jpeg;base64,"+Base64.getEncoder().encodeToString(data);
							
							user.setProfilePicture(base64);
							}
						
						
						thread.setUser(user);
						threadList.add(thread);
						
					}
				}
				
				if(i > 0 ) {
					
					return "success";
				}
				
				return "success";
				
			}catch(Exception e) {
				e.printStackTrace();
				return "failed";
			}
			
		}

	
	
	
	
	public Thread getThread() {
		return thread;
	}
	public void setThread(Thread thread) {
		this.thread = thread;
	}
	public List<Thread> getThreadList() {
		return threadList;
	}
	public void setThreadList(List<Thread> threadList) {
		this.threadList = threadList;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	
	
	

}
