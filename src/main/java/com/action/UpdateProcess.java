package com.action;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.sql.Blob;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.Map;

import javax.sql.rowset.serial.SerialBlob;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.struts2.interceptor.SessionAware;

import com.bean.Country;
import com.bean.User;
import com.google.common.hash.Hashing;
import com.models.CountryModel;
import com.models.UpdateProfileModel;
import com.models.UserModel;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class UpdateProcess extends ActionSupport implements SessionAware{

	private Map session = (Map) ActionContext.getContext().get("session");
	private User user;
	private UpdateProfileModel updateProfileModel;
	private Country country = null;
	ResultSet rs = null;
	private UserModel userModel = null;
	private List<Country> countryList = null;

	private boolean successMsg = false;
	private ArrayList<String> errorMsg = new ArrayList<String>();
	
	private File myFile;
	private String myFileContentType;
	private String myFileFileName;
    private String destPath;
	private String base64="";
    private String url; 

	public String execute() throws Exception{
		
		
	if(session.get("isLoggedIn") != null) {
//		 /* Copy file to a safe location */
	      destPath = "C:/Project_Asset/";
		try {
			
			//fetch user's profile
			String username = (String) session.get("username");
			
		
			
			//fetch country data
			CountryModel countryModel = new CountryModel();
			rs = countryModel.getCountry();
			
			if(rs != null) {
				
				countryList = new ArrayList<Country>();
				
				while(rs.next()) {
					country = new Country();
					country.setCountryId(rs.getString("c_id"));
					country.setCountryName(rs.getString("country_name"));
					countryList.add(country);
				}
			}
			
			
			//update process
			updateProfileModel = new UpdateProfileModel();
			if(user.getPassword().length() > 6 ){
				String hashPassword = Hashing.sha256().hashString(user.getPassword(), StandardCharsets.UTF_8).toString();
				user.setPassword(hashPassword);
			}else {
				errorMsg.add("Password must at least 6 characters!");
			}
			int insert = updateProfileModel.updateUserProfile(user,username);
			
			
			
			userModel = new UserModel();
			rs = userModel.getAllUserData(username);
			int i = 0;
			if(rs != null){
				while(rs.next()) {
					i++;
					user = new User();
					
					user.setUsername(rs.getString("username"));
					user.setFirstName(rs.getString("first_name"));
					user.setLastName(rs.getString("last_name"));
					user.setEmail(rs.getString("email"));
					user.setCountry(rs.getString("c_id"));
					
				if(rs.getBlob("profile_picture") != null) { 
					Blob c = rs.getBlob("profile_picture");
					byte[] data = c.getBytes(1, (int) c.length());

					String base64 = "data:image/jpeg;base64,"+Base64.getEncoder().encodeToString(data);
					user.setProfilePicture(base64);
				}
					
				}
			}
			
			//uploading image file
			if(myFile != null) {
				   FileInputStream iSteamReader = new FileInputStream(myFile);
			        byte[] imageBytes = IOUtils.toByteArray(iSteamReader);
			        Blob b = new SerialBlob(imageBytes);
		        base64 = "data:image/jpeg;base64,"+Base64.getEncoder().encodeToString(imageBytes);	
		        user.setProfilePicture(base64);
		        session.put("profile_picture", base64);
			 insert = updateProfileModel.updateUserImageProfile(username, b, myFile.length() );	
	
		    
	         
	         
			}
			
			if(insert > 0) {
				successMsg = true;
				url = "update-profile?successMsg=true";
				return "success";
			}
			
			return "failed";
			
		}catch(Exception e) {
			e.printStackTrace();
			return "failed";
		}
		
		
	 }else {
		 return "failed";
	 }
	
	
	}
	
	   public String convert(byte[] blob) {
	        String str = "";
	        //read bytes from ByteArrayInputStream using read method
	        if (blob != null && blob.length > 0) {
	            for (byte b : blob) {
	                str = str + (char) b;
	            }
	        }
	        return str;
	     }
	
	public Country getCountry() {
		return country;
	}



	public void setCountry(Country country) {
		this.country = country;
	}

	
	public File getMyFile() {
		return myFile;
	}



	public void setMyFile(File myFile) {
		this.myFile = myFile;
	}



	public String getMyFileContentType() {
		return myFileContentType;
	}



	public void setMyFileContentType(String myFileContentType) {
		this.myFileContentType = myFileContentType;
	}



	public String getMyFileFileName() {
		return myFileFileName;
	}



	public void setMyFileFileName(String myFileFileName) {
		this.myFileFileName = myFileFileName;
	}



	public List<Country> getCountryList() {
		return countryList;
	}


	public void setCountryList(List<Country> countryList) {
		this.countryList = countryList;
	}


	

	public String getDestPath() {
		return destPath;
	}


	public void setDestPath(String destPath) {
		this.destPath = destPath;
	}

	
	
	public Map getSession() {
		return session;
	}

	public void setSession(Map session) {
		this.session = session;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public ArrayList<String> getErrorMsg() {
		return errorMsg;
	}

	public void setErrorMsg(ArrayList<String> errorMsg) {
		this.errorMsg = errorMsg;
	}


	public boolean isSuccessMsg() {
		return successMsg;
	}

	public void setSuccessMsg(boolean successMsg) {
		this.successMsg = successMsg;
	}



	public String getBase64() {
		return base64;
	}



	public void setBase64(String base64) {
		this.base64 = base64;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
	
	
	
	

}
