package com.action;

import java.sql.Blob;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.Map;

import org.apache.struts2.dispatcher.SessionMap;
import org.apache.struts2.interceptor.SessionAware;

import com.bean.Country;
import com.bean.User;
import com.models.CountryModel;
import com.models.UserModel;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class UpdateProfile extends ActionSupport implements SessionAware{
	
	private SessionMap<String, Object> userSession;
	private Map session = (Map) ActionContext.getContext().get("session");
	private User user;
	private Country country = null;
	ResultSet rs = null;
	private UserModel userModel = null;
	private List<Country> countryList = null;
	private String successMsg;
	

	public List<Country> getCountryList() {
		return countryList;
	}


	public void setCountryList(List<Country> countryList) {
		this.countryList = countryList;
	}


	public void setSession(Map<String, Object> session) {
		// TODO Auto-generated method stub
		this.userSession = (SessionMap)session;
	}
	
	
	public String execute() throws Exception{
		
		try {
		
			if(session.get("isLoggedIn") != null) {
				String username = (String) session.get("username");
				
				userModel = new UserModel();
				rs = userModel.getAllUserData(username);
				int i = 0;
				if(rs != null){
					while(rs.next()) {
						i++;
						user = new User();
						
						user.setUsername(rs.getString("username"));
						user.setFirstName(rs.getString("first_name"));
						user.setLastName(rs.getString("last_name"));
						user.setEmail(rs.getString("email"));
						user.setCountry(rs.getString("c_id"));
						
						if(rs.getBlob("profile_picture") != null) { 
						//get user image and convert to base64
						Blob c = rs.getBlob("profile_picture");
						byte[] data = c.getBytes(1, (int) c.length());

						String base64 = "data:image/jpeg;base64,"+Base64.getEncoder().encodeToString(data);
						
						user.setProfilePicture(base64);
						session.put("userProfile", base64);
						}
						
				         
					}
				}
				
				CountryModel countryModel = new CountryModel();
				rs = countryModel.getCountry();
				
				if(rs != null) {
					
					countryList = new ArrayList<Country>();
					
					while(rs.next()) {
						country = new Country();
						country.setCountryId(rs.getString("c_id"));
						country.setCountryName(rs.getString("country_name"));
						countryList.add(country);
					}
				}
				
				
				if(i == 0) {
					return "failed";
				}
				
				
				
				return "success";
			}
			
			return "failed";
		}catch(Exception e) {
			e.printStackTrace();
			
			return "failed";
		}
		
		
	}


	public User getUser() {
		return user;
	}


	public void setUser(User user) {
		this.user = user;
	}


	public String getSuccessMsg() {
		return successMsg;
	}


	public void setSuccessMsg(String successMsg) {
		this.successMsg = successMsg;
	}
	
	
}
