package com.bean;

public class CommentJob {
	
	private String job_commet_id;
	private String job_id;
	private String account_id;
	private String response;
	private String response_date;
	private User user;
	
	
	public String getJob_commet_id() {
		return job_commet_id;
	}
	public void setJob_commet_id(String job_commet_id) {
		this.job_commet_id = job_commet_id;
	}
	public String getJob_id() {
		return job_id;
	}
	public void setJob_id(String job_id) {
		this.job_id = job_id;
	}
	public String getAccount_id() {
		return account_id;
	}
	public void setAccount_id(String account_id) {
		this.account_id = account_id;
	}
	public String getResponse() {
		return response;
	}
	public void setResponse(String response) {
		this.response = response;
	}
	public String getResponse_date() {
		return response_date;
	}
	public void setResponse_date(String response_date) {
		this.response_date = response_date;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}

}
