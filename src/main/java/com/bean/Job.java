package com.bean;

import java.util.ArrayList;
import java.util.List;

public class Job {
	
	private String jobId;
	private String accountId;
	private String jobTitle;
	private String category;
	private String description;
	private String start_date;
	private String end_date;
	private String post_date;
	private User user;
	private List<JobResponse> jobResponse = new ArrayList<JobResponse>();
	
	public List<JobResponse> getJobResponse() {
		return jobResponse;
	}
	public void setJobResponse(List<JobResponse> jobResponse) {
		this.jobResponse = jobResponse;
	}
	public String getJobId() {
		return jobId;
	}
	public void setJobId(String jobId) {
		this.jobId = jobId;
	}
	public String getAccountId() {
		return accountId;
	}
	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}
	public String getJobTitle() {
		return jobTitle;
	}
	public void setJobTitle(String jobTitle) {
		this.jobTitle = jobTitle;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getStart_date() {
		return start_date;
	}
	public void setStart_date(String start_date) {
		this.start_date = start_date;
	}
	public String getEnd_date() {
		return end_date;
	}
	public void setEnd_date(String end_date) {
		this.end_date = end_date;
	}
	public String getPost_date() {
		return post_date;
	}
	public void setPost_date(String post_date) {
		this.post_date = post_date;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}

	
}
