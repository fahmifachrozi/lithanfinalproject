package com.helpers;

import java.sql.ResultSet;


import com.models.ExistenceCheckerModel;

public class ExistenceChecker {
	
	ExistenceCheckerModel existenceChecker = new ExistenceCheckerModel();
	
	//to check if username exists
	public boolean usernameExists(String username) {
		ResultSet rs = null;
		int i = 0;
			try {
				rs =  existenceChecker.usernameExists(username);
				if(rs != null) {
					while(rs.next()) {
						i++;
					}
					
				}
				
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		
		return (i > 0) ? true : false;
	}
	
	//to check if email exists
	public boolean emailExists(String email){
		ResultSet rs = null;
		int i = 0;
			try {
				rs =  existenceChecker.emailExists(email);
				if(rs != null) {
					while(rs.next()) {
						i++;
					}
					
				}
				
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			return (i > 0) ? true : false;		
	}
	
	
	//to check if the user is already suspended or not
	public boolean isSuspended(String userInput){
		ResultSet rs = null;
		int i = 0;
			try {
				rs =  existenceChecker.isSuspended(userInput);
				if(rs != null) {
					while(rs.next()) {
						i++;
					}
					
				}
				
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			return (i > 0) ? true : false;		
	}
	
	
}
