package com.helpers;


import java.util.Properties;
import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class ResetPasswordMail {
	 	private String from = "abcjobemailtes@gmail.com";
	    private String password ="abcjob12345";
	    private String to = "";
	    private String subject ="[Reset Password] Abc Job Portal";
	    private String body  ="Your new password is =";
	 
	    static Properties properties = new Properties();
	    static
	    {
	       properties.put("mail.smtp.host", "smtp.gmail.com");
	       properties.put("mail.smtp.socketFactory.port", "465");
	       properties.put("mail.smtp.socketFactory.class",
	                      "javax.net.ssl.SSLSocketFactory");
	       properties.put("mail.smtp.auth", "true");
	       properties.put("mail.smtp.port", "465");
	    }
  

  
     //compose the message  
    public boolean send_email(String email, String newPassword)throws Exception{  
    	try {
    		this.to = email;
    		  Session session = Session.getDefaultInstance(properties,  
    		             new javax.mail.Authenticator() {
    		              protected PasswordAuthentication 
    		              getPasswordAuthentication() {
    		              return new
    		              PasswordAuthentication(from, password);
    		             }});
    		  Message message = new MimeMessage(session);
              message.setFrom(new InternetAddress(from));
              message.setRecipients(Message.RecipientType.TO, 
                 InternetAddress.parse(to));
              message.setSubject(subject);
              message.setText(body+" "+newPassword);
              Transport.send(message);
    		  
              return true;
    	}catch(Exception e) {
    		e.printStackTrace();
    		return false;
    	}

    }



	public String getFrom() {
		return from;
	}



	public void setFrom(String from) {
		this.from = from;
	}



	public String getPassword() {
		return password;
	}



	public void setPassword(String password) {
		this.password = password;
	}



	public String getSubject() {
		return subject;
	}



	public void setSubject(String subject) {
		this.subject = subject;
	}



	public String getTo() {
		return to;
	}



	public void setTo(String to) {
		this.to = to;
	}



	public String getBody() {
		return body;
	}



	public void setBody(String body) {
		this.body = body;
	}
}
