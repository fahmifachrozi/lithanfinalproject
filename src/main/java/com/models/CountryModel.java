package com.models;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class CountryModel {
	
	// method for create connection
	public static Connection getConnection()throws Exception{
		Connection con = null;
		try{
			Class.forName("com.mysql.jdbc.Driver");
			
			con = DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/portal_community","root","");
			
			return con;
		}catch(Exception e){
			
			e.printStackTrace();
			return null;
			
		}
	}

	public ResultSet getCountry() throws Exception{
		ResultSet rs = null;
		
		try {
			String sql ="SELECT * FROM country";
			PreparedStatement ps = getConnection().prepareStatement(sql);
			
			rs = ps.executeQuery();
			
			return rs;
		}catch(Exception e){
			e.printStackTrace();
			
			return null;
		}finally {
			if (getConnection() != null) {
				getConnection().close();
			}
		}
		
	}

}
