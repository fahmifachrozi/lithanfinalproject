package com.models;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class ExistenceCheckerModel {
	
	// method for create connection
	public static Connection getConnection()throws Exception{
		Connection con = null;
		try{
			Class.forName("com.mysql.jdbc.Driver");
			
			con = DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/portal_community","root","");
			
			return con;
		}catch(Exception e){
			
			e.printStackTrace();
			return null;
			
		}
	}
	
	
	public ResultSet usernameExists(String username) throws SQLException, Exception{
		ResultSet rs = null;
		
		try {
			String sql = "SELECT (username) FROM account WHERE username=?";
			PreparedStatement ps = getConnection().prepareStatement(sql);
			
			ps.setString(1, username);
			rs = ps.executeQuery();
			
			return rs;
			
		}catch(Exception e) {
			e.printStackTrace();
			return rs;
		}finally {
			if (getConnection() != null) {
				getConnection().close();
			}
		}
	}
	
	public ResultSet emailExists(String email) throws SQLException, Exception{
		ResultSet rs = null;
		
		try {
			String sql = "SELECT (email) FROM account WHERE email=?";
			PreparedStatement ps = getConnection().prepareStatement(sql);
			
			ps.setString(1, email);
			rs = ps.executeQuery();
			
			return rs;
			
		}catch(Exception e) {
			e.printStackTrace();
			return rs;
		}finally {
			if (getConnection() != null) {
				getConnection().close();
			}
		}
	}
	
	public ResultSet isSuspended(String userInput) throws SQLException, Exception{
		ResultSet rs = null;
		
		try {
			String sql = "SELECT * FROM account WHERE (email=? OR username=?) AND suspended='1'";
			PreparedStatement ps = getConnection().prepareStatement(sql);
			
			ps.setString(1, userInput);
			ps.setString(2, userInput);
			rs = ps.executeQuery();
			
			return rs;
			
		}catch(Exception e) {
			e.printStackTrace();
			return rs;
		}finally {
			if (getConnection() != null) {
				getConnection().close();
			}
		}
	}
	
	
	
}
