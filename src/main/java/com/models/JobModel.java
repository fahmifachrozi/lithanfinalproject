package com.models;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import com.bean.Job;


public class JobModel {
	
	
	// method for create connection
	public static Connection getConnection()throws Exception{
		Connection con = null;
		try{
			Class.forName("com.mysql.jdbc.Driver");
			
			con = DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/portal_community","root","");
			
			return con;
		}catch(Exception e){
			
			e.printStackTrace();
			return null;
			
		}
	}
	
	
	public int insertJob(Job job) throws Exception{
		int i = 0;
		
		try {
			
			String sql = "INSERT INTO job_opportunity (account_id,job_title,category,description,start_date,end_date) VALUES(?,?,?,?,?,?)";
			PreparedStatement ps = getConnection().prepareStatement(sql);
			
			
			ps.setString(1, job.getAccountId());
			ps.setString(2, job.getJobTitle());
			ps.setString(3, job.getCategory());
			ps.setString(4, job.getDescription());
			ps.setString(5, job.getStart_date());
			ps.setString(6, job.getEnd_date());
		

			
			i = ps.executeUpdate();
			return i;
			
		}catch(Exception e) {
			e.printStackTrace();
			return i;
		}finally {
			if (getConnection() != null) {
				getConnection().close();
			}
		}
	}	
	
	public ResultSet getAllJob() throws Exception{
		ResultSet rs = null;
			
			
		try {
			String sql = "SELECT a.profile_picture, a.username, a.first_name, a.last_name, a.role_id, jo.job_id, jo.account_id, jo.job_title, jo.category,SUBSTRING(jo.description, 1, 150) as description, jo.start_date, jo.end_date, jo.post_date FROM job_opportunity AS jo LEFT JOIN account AS a ON a.account_id = jo.account_id ORDER BY jo.post_date DESC";
			PreparedStatement ps = getConnection().prepareStatement(sql);
				
			rs = ps.executeQuery();
			
			return rs;
		}catch(Exception e) {
			e.printStackTrace();
			
			return null;
		}finally {
			if (getConnection() != null) {
				getConnection().close();
			}
		}
		
	}
	
	public ResultSet getDetailJob(String query) throws Exception{
		ResultSet rs = null;
			
			
		try {
			String sql = "SELECT a.profile_picture, a.username, a.first_name, a.last_name, a.role_id, jo.job_id, jo.account_id, jo.job_title, jo.category,jo.description, jo.start_date, jo.end_date, jo.post_date FROM job_opportunity AS jo LEFT JOIN account AS a ON a.account_id = jo.account_id WHERE jo.job_id=?";
			PreparedStatement ps = getConnection().prepareStatement(sql);
				
			ps.setString(1, query);
		
			
			rs = ps.executeQuery();
			
			return rs;
		}catch(Exception e) {
			e.printStackTrace();
			
			return null;
		}finally {
			if (getConnection() != null) {
				getConnection().close();
			}
		}
		
	}
	
	public ResultSet getRecentJob() throws Exception{
		ResultSet rs = null;
			
			
		try {
			String sql = "SELECT a.profile_picture, a.username, a.first_name, a.last_name, a.role_id, jo.job_id, jo.account_id, jo.job_title, jo.category,SUBSTRING(jo.description, 1, 50) as description, jo.start_date, jo.end_date, jo.post_date FROM job_opportunity AS jo LEFT JOIN account AS a ON a.account_id = jo.account_id ORDER BY jo.post_date DESC LIMIT 3";
			PreparedStatement ps = getConnection().prepareStatement(sql);
				
			rs = ps.executeQuery();
			
			return rs;
		}catch(Exception e) {
			e.printStackTrace();
			
			return null;
		}finally {
			if (getConnection() != null) {
				getConnection().close();
			}
		}
		
	}
	
	
	public int insertComment(String jobId,String comment, String account_id) throws Exception{
		int i = 0;
		
		try {
			
			String sql = "INSERT INTO job_opportunity_response (job_id,account_id,response) VALUES(?,?,?) ";
			PreparedStatement ps = getConnection().prepareStatement(sql);
			
			ps.setString(1, jobId);
			ps.setString(2, account_id);
			ps.setString(3, comment);
		

			
			i = ps.executeUpdate();
			return i;
			
		}catch(Exception e) {
			e.printStackTrace();
			return i;
			
		}finally {
			if (getConnection() != null) {
				getConnection().close();
			}
		}
	}	
	
	public ResultSet getCommentJob(String jobId) throws Exception{
		ResultSet rs = null;
			
			
		try {
			String sql = "SELECT a.profile_picture, a.username, a.first_name, a.last_name,jor.* FROM job_opportunity_response AS jor LEFT JOIN account AS a ON a.account_id=jor.account_id WHERE job_id=? ORDER BY jor.response_date ASC";
			PreparedStatement ps = getConnection().prepareStatement(sql);
			ps.setString(1, jobId);
			rs = ps.executeQuery();
			
			return rs;
		}catch(Exception e) {
			e.printStackTrace();
			
			return null;
		}finally {
			if (getConnection() != null) {
				getConnection().close();
			}
		}
		
	}
	
}
