package com.models;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import com.bean.User;

public class MessageModel {

	
	// method for create connection
			public static Connection getConnection()throws Exception{
				Connection con = null;
				try{
					Class.forName("com.mysql.jdbc.Driver");
					
					con = DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/portal_community","root","");
					
					return con;
				}catch(Exception e){
					
					e.printStackTrace();
					return null;
					
				}
			}

			
			public ResultSet getMessageHistory(String friendId, String myId) throws Exception{
				ResultSet rs = null;
					
					
				try {
					String sql = "SELECT m.*,a.* FROM message AS m LEFT JOIN account AS a ON a.account_id = m.sender_id WHERE (m.sender_id=? OR m.receiver_id=?) AND (m.sender_id=? OR m.receiver_id=?)";
					PreparedStatement ps = getConnection().prepareStatement(sql);
						
					 ps.setString(1, friendId);
					 ps.setString(2, friendId);
					 ps.setString(3, myId);
					 ps.setString(4, myId);
					
					rs = ps.executeQuery();
					
					return rs;
				}catch(Exception e) {
					e.printStackTrace();
					
					return null;
				}finally {
					if (getConnection() != null) {
						getConnection().close();
					}
				}
				
			}
			
			
			public int insertMessage(String message, String friendId, String myId) throws Exception{
				int i = 0;
				
				try {
					
					String sql = "INSERT INTO message (sender_id,receiver_id, message) VALUES(?,?,?)";
					PreparedStatement ps = getConnection().prepareStatement(sql);
					
					ps.setString(1, myId);
					ps.setString(2, friendId);
					ps.setString(3, message);
			
					
					i = ps.executeUpdate();
					return i;
					
				}catch(Exception e) {
					e.printStackTrace();
					return i;
				}finally {
					if (getConnection() != null) {
						getConnection().close();
					}
				}
			}	
			
			
			public ResultSet getMessageList(String myId) throws Exception{
				ResultSet rs = null;
					
					
				try {
					String sql = "SELECT *,(r.sender_id + r.receiver_id) AS myid FROM (SELECT * FROM message AS m WHERE (m.sender_id = ? OR m.receiver_id = ?) )r GROUP BY myid";
					PreparedStatement ps = getConnection().prepareStatement(sql);
						
					 ps.setString(1, myId);
					 ps.setString(2, myId);
					
					rs = ps.executeQuery();
					
					return rs;
				}catch(Exception e) {
					e.printStackTrace();
					
					return null;
				}finally {
					if (getConnection() != null) {
						getConnection().close();
					}
				}
				
			}
			
			
}
