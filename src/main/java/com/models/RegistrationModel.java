package com.models;


import java.sql.*;
//import java.sql.Connection;
//import java.sql.DriverManager;
//import java.sql.PreparedStatement;

import com.bean.User;



public class RegistrationModel {
	
	
	// method for create connection
	public static Connection getConnection()throws Exception{
		Connection con = null;
		try{
			Class.forName("com.mysql.jdbc.Driver");
			
			con = DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/portal_community","root","");
			
			return con;
		}catch(Exception e){
			
			e.printStackTrace();
			return null;
			
		}
	}

	
	public int register(User user) throws Exception{
		int i = 0;
		
		try {
			
			String sql = "INSERT INTO account (role_id,first_name,last_name,email,username,password) VALUES(?,?,?,?,?,?)";
			PreparedStatement ps = getConnection().prepareStatement(sql);
			
			ps.setString(1, "1");
			ps.setString(2, user.getFirstName());
			ps.setString(3, user.getLastName());
			ps.setString(4, user.getEmail());
			ps.setString(5, user.getUsername());
			ps.setString(6, user.getPassword());
			
			i = ps.executeUpdate();
			return i;
			
		}catch(Exception e) {
			e.printStackTrace();
			return i;
		}
		finally {
			if (getConnection() != null) {
				getConnection().close();
			}
		}
	}	
	
	
}
