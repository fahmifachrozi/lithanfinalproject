package com.models;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import com.bean.Thread;

public class ThreadModel {
	

	// method for create connection
	public static Connection getConnection()throws Exception{
		Connection con = null;
		try{
			Class.forName("com.mysql.jdbc.Driver");
			
			con = DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/portal_community","root","");
			
			return con;
		}catch(Exception e){
			
			e.printStackTrace();
			return null;
			
		}
	}
	
	
	public int insertThread(Thread thread) throws Exception{
		int i = 0;
		
		try {
			
			String sql = "INSERT INTO thread ( account_id, title, content) VALUES(?,?,?)";
			PreparedStatement ps = getConnection().prepareStatement(sql);
			
			
			ps.setString(1, thread.getAccountId());
			ps.setString(2, thread.getTitle());
			ps.setString(3, thread.getContent());
			
		
			
			i = ps.executeUpdate();
			return i;
			
		}catch(Exception e) {
			e.printStackTrace();
			return i;
		}
		finally {
			if (getConnection() != null) {
				getConnection().close();
			}
		}
	}	
	
	
	public ResultSet getAllThread() throws Exception{
		ResultSet rs = null;
			
			
		try {
			String sql = "SELECT a.profile_picture, a.username, a.first_name, a.last_name, a.role_id, t.thread_id, t.title, SUBSTRING(t.content, 1, 200) as content, t.post_date FROM thread AS t LEFT JOIN account AS a ON a.account_id = t.account_id ORDER BY t.post_date DESC";
			PreparedStatement ps = getConnection().prepareStatement(sql);
				
			rs = ps.executeQuery();
			
			return rs;
		}catch(Exception e) {
			e.printStackTrace();
			
			return null;
		}finally {
			if (getConnection() != null) {
				getConnection().close();
			}
		}
		
	}
	
	public ResultSet getDetailThread(String query) throws Exception{
		ResultSet rs = null;
			
			
		try {
			String sql = "SELECT a.profile_picture, a.username, a.first_name, a.last_name, a.role_id, t.thread_id, t.title, t.content, t.post_date FROM thread AS t LEFT JOIN account AS a ON a.account_id = t.account_id WHERE t.thread_id=?";
			PreparedStatement ps = getConnection().prepareStatement(sql);
				
			ps.setString(1, query);
		
			
			rs = ps.executeQuery();
			
			return rs;
		}catch(Exception e) {
			e.printStackTrace();
			
			return null;
		}finally {
			if (getConnection() != null) {
				getConnection().close();
			}
		}
		
	}
	
	
	public ResultSet getUserThread(String user_id) throws Exception{
		ResultSet rs = null;
			
			
		try {
			String sql = "SELECT  t.thread_id, t.title, SUBSTRING(t.content, 1, 300) as content, t.post_date FROM thread AS t  WHERE t.account_id=? ORDER BY t.post_date DESC";
			PreparedStatement ps = getConnection().prepareStatement(sql);
			ps.setString(1, user_id);
			rs = ps.executeQuery();
			
			return rs;
		}catch(Exception e) {
			e.printStackTrace();
			
			return null;
		}finally {
			if (getConnection() != null) {
				getConnection().close();
			}
		}
		
	}
	
	public ResultSet getRecentThread() throws Exception{
		ResultSet rs = null;
			
			
		try {
			String sql = "SELECT a.profile_picture, a.username, a.first_name, a.last_name, a.role_id, t.thread_id, t.title, SUBSTRING(t.content, 1, 50) as content, t.post_date FROM thread AS t LEFT JOIN account AS a ON a.account_id = t.account_id ORDER BY t.post_date DESC LIMIT 3";
			PreparedStatement ps = getConnection().prepareStatement(sql);
				
			rs = ps.executeQuery();
			
			return rs;
		}catch(Exception e) {
			e.printStackTrace();
			
			return null;
		}finally {
			if (getConnection() != null) {
				getConnection().close();
			}
		}
		
	}
	
}
