package com.models;

import java.io.FileInputStream;
import java.io.InputStream;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;

import com.bean.User;

public class UpdateProfileModel {
		
	// method for create connection
	public static Connection getConnection()throws Exception{
		Connection con = null;
		try{
			Class.forName("com.mysql.jdbc.Driver");
			
			con = DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/portal_community","root","");
			
			return con;
		}catch(Exception e){
			
			e.printStackTrace();
			return null;
			
		}
	}
	
	
	public int updateUserProfile(User user, String username) throws Exception{
		getConnection().setAutoCommit(false);
		
		int i = 0;
		
		try {
			
			if(user.getPassword().length() > 6) {
				String sql = "UPDATE account SET c_id=?,first_name=?,last_name=?,password=? WHERE username=?";
				PreparedStatement ps = getConnection().prepareStatement(sql);
				
				ps.setString(1, user.getCountry());
				ps.setString(2, user.getFirstName());
				ps.setString(3, user.getLastName());
				ps.setString(4, user.getPassword());
				ps.setString(5, username);
				
				i = ps.executeUpdate();
			}else {
				String sql = "UPDATE account SET c_id=?,first_name=?,last_name=? WHERE username=?";
				PreparedStatement ps = getConnection().prepareStatement(sql);
				
				ps.setString(1, user.getCountry());
				ps.setString(2, user.getFirstName());
				ps.setString(3, user.getLastName());
				ps.setString(4, username);
				
				i = ps.executeUpdate();
			}
			
		
			
		
			
		
			
			return i;
		}catch(Exception e) {
			e.printStackTrace();
			getConnection().rollback();
			return 0;
			
		}finally {
			if (getConnection() != null) {
				getConnection().close();
			}
		}
		
	}
	
	
	public int updateUserImageProfile(String username,Blob image, long longFile) throws Exception {
	getConnection().setAutoCommit(false);
		
		int i = 0;
		
		try {
			
			
			String sql = "UPDATE account SET profile_picture=? WHERE username=?";
			PreparedStatement ps = getConnection().prepareStatement(sql);
			
			ps.setBlob(1, image);
		
			ps.setString(2, username);
		
			
			i = ps.executeUpdate();
			
		
			
			return i;
		}catch(Exception e) {
			e.printStackTrace();
			getConnection().rollback();
			return 0;
			
		}finally {
			if (getConnection() != null) {
				getConnection().close();
			}
		}
	}
	
	
}
