package com.models;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import com.bean.User;

public class UserModel {

	// method for create connection
		public static Connection getConnection()throws Exception{
			Connection con = null;
			try{
				Class.forName("com.mysql.jdbc.Driver");
				
				con = DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/portal_community","root","");
				
				return con;
			}catch(Exception e){
				
				e.printStackTrace();
				return null;
				
			}
		}

		
		public int register(User user) throws Exception{
			int i = 0;
			
			try {
				
				String sql = "INSERT INTO account (role_id,first_name,last_name,email,username,password) VALUES(?,?,?,?,?,?)";
				PreparedStatement ps = getConnection().prepareStatement(sql);
				
				ps.setString(1, "1");
				ps.setString(2, user.getFirstName());
				ps.setString(3, user.getLastName());
				ps.setString(4, user.getEmail());
				ps.setString(5, user.getUsername());
				ps.setString(6, user.getPassword());
				
				i = ps.executeUpdate();
				return i;
				
			}catch(Exception e) {
				e.printStackTrace();
				return i;
			}finally {
				if (getConnection() != null) {
					getConnection().close();
				}
			}
		}	
		
		
		public ResultSet getUser(String usernameOrEmail, String password) throws Exception{
			ResultSet rs = null;
				
				
			try {
				String sql = "SELECT * FROM account LEFT JOIN country ON country.c_id = account.c_id WHERE (account.username=? OR account.email=?) AND account.password=? AND suspended!='1'";
				PreparedStatement ps = getConnection().prepareStatement(sql);
					
				ps.setString(1, usernameOrEmail);
				ps.setString(2, usernameOrEmail);
				ps.setString(3, password);
				
				rs = ps.executeQuery();
				
				return rs;
			}catch(Exception e) {
				e.printStackTrace();
				
				return null;
			}finally {
				if (getConnection() != null) {
					getConnection().close();
				}
			}
			
		}
		
		public ResultSet getAllUserData(String username) throws Exception{
			ResultSet rs = null;
				
				
			try {
				String sql = "SELECT * FROM account LEFT JOIN country ON country.c_id = account.c_id WHERE account.username=?";
				PreparedStatement ps = getConnection().prepareStatement(sql);
					
				ps.setString(1, username);
				
				rs = ps.executeQuery();
				
				return rs;
			}catch(Exception e) {
				e.printStackTrace();
				
				return null;
			}finally {
				if (getConnection() != null) {
					getConnection().close();
				}
			}
			
		}
		
		
		public ResultSet searchUser(String query) throws Exception{
			ResultSet rs = null;
				
				
			try {
				String sql = "SELECT * FROM account LEFT JOIN country ON country.c_id = account.c_id WHERE account.first_name LIKE ? OR account.last_name LIKE ? OR country.country_name LIKE ?";
				PreparedStatement ps = getConnection().prepareStatement(sql);
					
				ps.setString(1, "%"+query+"%");
				ps.setString(2, "%"+query+"%");
				ps.setString(3, "%"+query+"%");
				
				rs = ps.executeQuery();
				
				return rs;
			}catch(Exception e) {
				e.printStackTrace();
				
				return null;
			}finally {
				if (getConnection() != null) {
					getConnection().close();
				}
			}
			
		}
		
		
		public ResultSet getUserById(String idUser) throws Exception{
			ResultSet rs = null;
				
				
			try {
				String sql = "SELECT * FROM account LEFT JOIN country ON country.c_id = account.c_id WHERE account.account_id=?";
				PreparedStatement ps = getConnection().prepareStatement(sql);
					
				ps.setString(1, idUser);
			
				
				rs = ps.executeQuery();
				
				return rs;
			}catch(Exception e) {
				e.printStackTrace();
				
				return null;
			}finally {
				if (getConnection() != null) {
					getConnection().close();
				}
			}
			
		}
		
		
		public ResultSet getAllUser() throws Exception{
			ResultSet rs = null;
				
				
			try {
				String sql = "SELECT * FROM account LEFT JOIN country ON country.c_id = account.c_id ";
				PreparedStatement ps = getConnection().prepareStatement(sql);
					
				
				rs = ps.executeQuery();
				
				return rs;
			}catch(Exception e) {
				e.printStackTrace();
				
				return null;
			}finally {
				if (getConnection() != null) {
					getConnection().close();
				}
			}
			
		}
		
		public int suspendUser(String username) throws Exception {
			getConnection().setAutoCommit(false);
				
				int i = 0;
				
				try {
					
					
					String sql = "UPDATE account SET suspended='1' WHERE username=?";
					PreparedStatement ps = getConnection().prepareStatement(sql);
					
				
					ps.setString(1, username);
				
					
					i = ps.executeUpdate();
					
				
					
					return i;
				}catch(Exception e) {
					e.printStackTrace();
					getConnection().rollback();
					return 0;
					
				}finally {
					if (getConnection() != null) {
						getConnection().close();
					}
				}
			}
		
		
		public int resetPassword(String email, String password) throws Exception {
			getConnection().setAutoCommit(false);
				
				int i = 0;
				
				try {
					
					
					String sql = "UPDATE account SET password=? WHERE email=?";
					PreparedStatement ps = getConnection().prepareStatement(sql);
					
					ps.setString(1, password);
					ps.setString(2, email);
				
					
					i = ps.executeUpdate();
					
				
					
					return i;
				}catch(Exception e) {
					e.printStackTrace();
					getConnection().rollback();
					return 0;
					
				}finally {
					if (getConnection() != null) {
						getConnection().close();
					}
				}
			}
		
		
}
