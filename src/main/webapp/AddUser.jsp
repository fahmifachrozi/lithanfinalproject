<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
<title>ABC Jobs Portal - Welcome</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<script
	src="https://cdn.ckeditor.com/ckeditor5/23.0.0/inline/ckeditor.js"></script>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="css/style.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
<style>
.card {
	width: 25rem;
	text-align: left;
}

body {
	font-size: 14px;
}
</style>
</head>

<body>
	 <s:include value="/WEB-INF/header/header.jsp"/>

	<div class="container fluid mt-3" style="min-height: 500px;">
		<div class="row">
			<h2>User Management</h2>

			<div class="col-sm-12">

				<div class="col-sm-12 text-center">
					<div class="col-sm-6 col-md-6 col-lg-6" style="margin: auto;">
						<h4 class="text-info">Add User</h4>
						<form>
							<div class="form-group mb-4">

								<select class="form-control">
									<option>Role Access</option>
									<option>Software Programmer</option>
									<option>Administrator</option>
								</select>
							</div>
							<div class="form-group mb-4">
								<input type="text" class="form-control" id="exampleInputEmail1"
									aria-describedby="emailHelp" placeholder="Enter Username">
							</div>
							<div class="form-group mb-4">

								<input type="text" class="form-control" id="exampleInputEmail1"
									aria-describedby="emailHelp" placeholder="Enter First Name">
							</div>
							<div class="form-group mb-4">

								<input type="text" class="form-control" id="exampleInputEmail1"
									aria-describedby="emailHelp" placeholder="Enter Last Name">
							</div>
							<div class="form-group mb-4">

								<input type="email" class="form-control" id="exampleInputEmail1"
									aria-describedby="emailHelp" placeholder="Enter email">

							</div>
							<div class="form-group mb-5">

								<input type="password" class="form-control"
									id="exampleInputPassword1" placeholder="Password">
							</div>
							<button type="submit" class="btn btn-primary"
								style="width: 100%;">Add User</button>
						</form>



					</div>
				</div>
			</div>
		</div>

	</div>

 <s:include value="/WEB-INF/footer/footer.jsp"/>

</body>
</html>