<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
<title>ABC Jobs Portal - Welcome</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<script
	src="https://cdn.ckeditor.com/ckeditor5/23.0.0/inline/ckeditor.js"></script>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="css/style.css">
<link rel="stylesheet" href="css/card.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

</head>

<body>

	<s:if test="#session.isLoggedIn != null">
		<s:include value="/WEB-INF/header/header.jsp"/>
	</s:if>
	<s:else>
		<s:include value="/WEB-INF/header/LoginHeader.jsp"/>
	</s:else>
	
	<div class="container-fluid mt-3 mb-2" style="min-height:300px;">
		<div class="row">

			<div class="col-sm-12 col-md-10 col-lg-10" style="margin: auto;">
				<h3>${job.jobTitle.toUpperCase()}</h3>
				<div class="card" style="width: 100%;">
					<div class="card-header">
								<s:if test="job.user.profilePicture == null">
										<img src="images/avatar.png" class="rounded-circle"
										style="width: 50px; float: left; margin-right: 10px;">
								</s:if>
								<s:else>
										<img src="${job.user.profilePicture}" class="rounded-circle"
										style="width: 50px; height:50px; float: left; margin-right: 10px;">
								</s:else>
					

						<div>
							<span>${job.user.firstName.toUpperCase()} ${job.user.lastName.toUpperCase()}</span>
						</div>
						<div>
							<span>Job Period: ${job.start_date} <b>To</b> ${job.end_date }</span>
						</div>
					</div>
					<div class="card-body">
						${job.description}
					</div>
					<div class="card-footer">
						<p>Posted On: ${job.post_date}</p>
					</div>
				</div>
			</div>

		</div>
	</div>

	<!-- comment -->

	<div class="container-fluid">
		<div class="row ">


			<div class="col-lg-10" style="margin: auto;">
				<form action="comment-job" method="POST">
				<div class="col-lg-6 col-md-12 comment-main rounded">
					<ul class="p-0">
					
				<s:iterator value="commentJobList">
						<li>
							<div class="row comment-box p-1 pt-3 pr-4">
								<div class="col-lg-2 col-3 user-img text-center">
								<s:if test="user.profilePicture == null">
									<img src="images/avatar.png" class="main-cmt-img">
								</s:if>
								<s:else>
									<img src="${user.profilePicture}" class="main-cmt-img">
								</s:else>
								</div>
								<div class="col-lg-10 col-9 user-comment bg-light rounded pb-1">
									<div class="row">
										<div class="col-lg-8 col-6 border-bottom pr-0">
											<a href="profile?username=${user.username}"><p class="w-100 p-2 m-0 text-secondary">${user.firstName} ${user.lastName}</p></a>
											<p class="w-100 p-2 m-0">${response}</p>
										</div>
										<div class="col-lg-4 col-6 border-bottom">
											<p class="w-100 p-2 m-0">
												<span class="float-right"><i
													class="fa fa-clock-o mr-1" aria-hidden="true"></i>${response_date}</span>
											</p>
										</div>
									</div>
									
								</div>
							</div>			
		
						</li>
					</s:iterator>
		
				
						<hr>
					<s:if test="#session.isLoggedIn == true">	
						<div class="row">
					
							<div class="col-lg-10 col-10">
							<input type="hidden" name="jobId" value="${q}">
								<input type="text" class="form-control" name="comment"
									placeholder="write comments ...">
							</div>
							<div class="col-lg-2 col-2 send-icon">
								<button type="submit" style="background-color:grey; width:0; margin-left:-20px;"><i class="fa fa-paper-plane" aria-hidden="true"></i></button>
							</div>
						</div>
					</s:if>
					<s:else>
						<div class="row">
					
							<div class="col-lg-10 col-10">
							
								<input type="text" class="form-control" name="comment"
									placeholder="You must be logged in to post a comment!" disabled>
							</div>
							<div class="col-lg-2 col-2 send-icon">
								<button  style="background-color:grey; width:0; margin-left:-20px; margin-top:-5px;" disabled><i class="fa fa-lock" aria-hidden="true" style="font-size:2rem;"></i></button>
							</div>
						</div>
					
					</s:else>	
					</ul>
					</form>
				</div>
			</div>
		
		<!-- end of comment -->


 	<s:include value="/WEB-INF/footer/footer.jsp"/>
 	
</body>
</html>