<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
<title>ABC Jobs Portal - Welcome</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<script
	src="https://cdn.ckeditor.com/ckeditor5/23.0.0/inline/ckeditor.js"></script>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="css/style.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
<style>
.card {
	width: 25rem;
	text-align: left;
}

body {
	font-size: 14px;
}
</style>
</head>

<body>

	<s:if test="#session.isLoggedIn != null">
		<s:include value="/WEB-INF/header/header.jsp"/>
	</s:if>
	<s:else>
		<s:include value="/WEB-INF/header/LoginHeader.jsp"/>
	</s:else>
	
	<div class="container fluid mt-3" style="min-height: 500px;">
		<div class="row">
			<div class="col-sm-12">
				<h3>Available Jobs</h3>
			</div>
			
			<div class="col-sm-12 text-right mb-2">
			<s:if test="#session.isLoggedIn">
				<a href="job-post" class="btn btn-danger"
					style="width: 150px;">Post a job <i
					class="fa fa-pencil-square-o"></i></a>
			</s:if>
			<s:else>
				<a href="login.jsp" class="btn btn-danger"
					style="width: 150px;">Post a job <i
					class="fa fa-pencil-square-o"></i></a>
			</s:else>
			</div>
		<s:iterator value="jobList">
		
			<div class="col-sm-4 col-md-4 col-lg-3 mt-2">
				<div class="card" style="width: 17rem;">
					<s:if test="user.profilePicture == null">
						<img class="card-img-top mt-3" src="images/avatar.png"
								alt="Card image cap"
								style="width: 190px; height: 200px; margin: auto;">
					</s:if>
					<s:else>
						<img class="card-img-top mt-3" src="${user.profilePicture}"
								alt="Card image cap"
								style="width: 190px; height: 200px; margin: auto; ">
					</s:else>
					<div class="card-body">
						<a href="detail-job?q=${jobId}">
							<h5 class="card-title">${jobTitle.toUpperCase()}</h5>
						</a>
						<p class="card-text">
							${description}...
						</p>
						<p style="font-size:0.8rem;">Posted by: ${user.firstName} ${user.lastName}</p>
						<p style="font-size:0.8rem;">Posted on: ${post_date}</p>
						<a href="detail-job?q=${jobId}" class="btn btn-primary">See Detail</a>
					</div>
				</div>
			</div>

		</s:iterator>

		</div>
	</div>

 <s:include value="/WEB-INF/footer/footer.jsp"/>

</body>
</html>