<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
<title>ABC Jobs Portal - Welcome</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<script
	src="https://cdn.ckeditor.com/ckeditor5/23.0.0/inline/ckeditor.js"></script>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="css/style.css">
<link rel="stylesheet" href="css/message.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

</head>

<body>

	<s:include value="/WEB-INF/header/header.jsp"/>
	
<div class="container mt-3"  style=" min-height:370px;">
<div class="row">

<div class="col-sm-10 col-md-10 col-lg-10" style="margin:auto;">	

	<h4>Message History</h4>
  <div class="row rounded-lg overflow-hidden shadow">
    <!-- Users box-->



    <!-- Chat Box-->
    <div class="col-12 px-0">
      <div class="px-4 py-5 chat-box bg-white">
      
      
   

<s:iterator value="listMessageHistory">
	<s:if test="sender_id == friendId ">
		  <!-- Sender Message-->
        <div class="media w-50 mb-3">
        
        <s:if test="user.profilePicture == null">
         	<img src="images/avatar.png" alt="user" width="50" class="rounded-circle">
        </s:if>
        <s:else>
        	<img src="${user.profilePicture}" alt="user" width="50" height="50" class="rounded-circle">
        </s:else>
       
          <div class="media-body ml-3">
            <div class="bg-light rounded py-2 px-3 mb-2">
              <p class="text-small mb-0 text-muted">${message}</p>
            </div>
            <p class="small text-muted">${user.firstName} ${user.lastName} - ${time}</p>
          </div>
        </div>
	</s:if>
	<s:else>
	
		  <!-- Reciever Message-->
        <div class="media w-50 ml-auto mb-3">
          <div class="media-body">
            <div class="bg-primary rounded py-2 px-3 mb-2">
              <p class="text-small mb-0 text-white">${message}</p>
            </div>
            <p class="small text-muted">${time}</p>
          </div>
        </div>
	
	</s:else>
     
</s:iterator>
     

      </div>

      <!-- Typing area -->
      <form action="send-message" method="POST" class="bg-light" >
        <div class="input-group">
        	<input type="hidden" name="friendId" value="${friendId}">
          <input type="text" name="message" placeholder="Type a message" aria-describedby="button-addon2" class="form-control rounded-0 border-0 py-4 bg-light">
          <div class="input-group-append">
            <button id="button-addon2" type="submit" class="btn btn-danger"> <i class="fa fa-paper-plane" style="font-size:25px;"></i></button>
          </div>
        </div>
      </form>



</div>
</div>

		</div>
	</div>
</div>

 <s:include value="/WEB-INF/footer/footer.jsp"/>

</body>
</html>