<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
<title>ABC Jobs Portal - Welcome</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">

<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="css/style.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

</head>

<body>
	
	<s:if test="#session.isLoggedIn != null">
		<s:include value="/WEB-INF/header/header.jsp"/>
	</s:if>
	<s:else>
		<s:include value="/WEB-INF/header/LoginHeader.jsp"/>
	</s:else>
	
	<div class="container mt-3" style="min-height: 500px;">
		<div class="row justify-content-center">
			<!--end of col-->
			<div class="col-12 col-md-10 col-lg-8 mt-3">
			<h4>Message History</h4>
				<table class="table">
				<thead>
					<th>Profile Pic</th>
					<th>Username</th>
					<th>Full Name</th>
				
					<th></th>
				</thead>
					<tbody>
					
					<s:iterator value="listMessageHistory">
						<tr>
							<td>
							<s:if test="user.profilePicture == null">
								<img src="images/avatar.png" class="img-list rounded-circle">
							</s:if>
							<s:else>
								<img src="${user.profilePicture}" class=" rounded-circle" width="60" height="60">
							</s:else>
							
							</td>
							<td>${user.username}</td>
							<td>${user.firstName}
							${user.lastName}</td>
							<td><a href="message-history?friendId=${user.user_id}" class="btn  btn-primary"><i
									class="fa fa-comments-o" style="font-size: 30px"></i></a></td>

						</tr>
				</s:iterator>
					</tbody>
				</table>
			</div>
		</div>

	</div>

 <s:include value="/WEB-INF/footer/footer.jsp"/>

</body>
</html>