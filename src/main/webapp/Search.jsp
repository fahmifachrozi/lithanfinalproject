<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
<title>ABC Jobs Portal - Welcome</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<script
	src="https://cdn.ckeditor.com/ckeditor5/23.0.0/inline/ckeditor.js"></script>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="css/style.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

</head>

<body>

	<s:if test="#session.isLoggedIn != null">
		<s:include value="/WEB-INF/header/header.jsp"/>
	</s:if>
	<s:else>
		<s:include value="/WEB-INF/header/LoginHeader.jsp"/>
	</s:else>
	
	<div class="container mt-3" style="min-height: 500px;">
		<div class="row justify-content-center">
			<div class="col-12 col-md-10 col-lg-8">
				<form class="card card-sm" action="search" method="GET">
					<div class="card-body row no-gutters align-items-center">
						<div class="col-auto">
							<i class="fa fa-search h4 text-body" style="font-size: 24px;"></i>
							&nbsp;
						</div>
						<!--end of col-->
						<div class="col">
							<input
								class="form-control form-control-lg form-control-borderless"
								type="search" name="query" value="${query}" placeholder="Search others..">
						</div>
						<!--end of col-->
						<div class="col-md-2">
							<button class="btn btn-lg btn-success" type="submit">Search</button>
						</div>
						<!--end of col-->
					</div>
				</form>
			</div>
			<!--end of col-->
			<div class="col-12 col-md-10 col-lg-8 mt-3">
				<table class="table">
					<thead>
						<tr>
							<th scope="col">#</th>
							<th scope="col">Profile Picture</th>
							<th scope="col">First Name</th>
							<th scope="col">Last Name</th>
							<th scope="col">Country</th>
							<th scope="col"></th>
							
						</tr>
					</thead>
					<tbody>
					
					<s:iterator value="userList" status="testStatus">
					
						<tr>
							<th scope="row"><s:property value="#testStatus.count"/></th>
							<td>
							<s:if test="profilePicture != null">
								<img src="<s:property value="profilePicture" />" style="width:60px; height:40px;">
							</s:if>
							<s:else>
								<img src="images/avatar.png" style="width:60px; height:40px;">
							</s:else>
								
							
							</td>
							<td>${firstName}</td>
							<td>${lastName}</td>
							<td>${countryName}</td>
							<td>
							<a href="profile?username=${username}" class="btn btn-danger">Profile</a>
							<s:if test="#session.isLoggedIn != null">
							<s:if test="#session.username != username">
							<a href="message-history?friendId=${user_id}" class="btn btn-primary">
									<i class="fa fa-comments-o" style=" font-size: 1.5rem;"></i>
								</a>
							</s:if>
							</s:if>
							</td>
							
						</tr>
						
				</s:iterator>
				
				</table>
			</div>
		</div>

	</div>

 <s:include value="/WEB-INF/footer/footer.jsp"/>

</body>
</html>