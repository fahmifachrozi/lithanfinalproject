<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
<title>ABC Jobs Portal - Welcome</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<script
	src="https://cdn.ckeditor.com/ckeditor5/23.0.0/inline/ckeditor.js"></script>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="css/style.css">
<link rel="stylesheet" href="css/card.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

</head>

<body>
	<s:if test="#session.isLoggedIn != null">
		<s:include value="/WEB-INF/header/header.jsp"/>
	</s:if>
	<s:else>
		<s:include value="/WEB-INF/header/LoginHeader.jsp"/>
	</s:else>
	
	<div class="container-fluid mt-3 mb-2" style="min-height:450px;">
		<div class="row">

			<div class="col-sm-12 col-md-10 col-lg-10" style="margin: auto;">
				<h3>${thread.title.toUpperCase()}</h3>
				<div class="card" style="width: 100%;">
					<div class="card-header">
					<s:if test="thread.user.profilePicture == null">
						<img src="images/avatar.png" class="rounded-circle"
							style="width: 50px; float: left; margin-right: 10px;">
					</s:if>
					<s:else>
						<img src="${thread.user.profilePicture}" class="rounded-circle"
							style="width: 50px; height:50px; float: left; margin-right: 10px;">
					</s:else>
						<div>
							<span>${thread.user.firstName.toUpperCase()} ${thread.user.lastName.toUpperCase()}</span>
						
						</div>
						
					</div>
					<div class="card-body">${thread.content}</div>
					<div class="card-footer">
						<p>Posted On: ${thread.post_date}</p>
					</div>
				</div>
			</div>

		</div>
	</div>



<s:include value="/WEB-INF/footer/footer.jsp"/>
	
</body>
</html>