<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
<title>ABC Jobs Portal - Welcome</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<script
	src="https://cdn.ckeditor.com/ckeditor5/23.0.0/inline/ckeditor.js"></script>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="css/style.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
<style>
.card {
	width: 25rem;
	text-align: left;
}

body {
	font-size: 14px;
}
</style>
</head>

<body>
	<s:if test="#session.isLoggedIn != null">
		<s:include value="/WEB-INF/header/header.jsp"/>
	</s:if>
	<s:else>
		<s:include value="/WEB-INF/header/LoginHeader.jsp"/>
	</s:else>
	
	<div class="container fluid mt-3" style="min-height: 500px;">
		<div class="row">
			<div class="col-sm-12">
				<h3>Thread List</h3>
			</div>
			<div class="col-sm-12 text-right mb-2">
				<a href="thread-post" class="btn btn-danger"
					style="width: 150px;">Post Thread <i
					class="fa fa-pencil-square-o"></i></a>
			</div>
			
			<s:iterator value="threadList">
			<!-- list -->
			<div class="card" style="width: 100%;">
				<div class="card-body">
					<div class="container">

						<div class="row">
							<div class="col-sm-2">
							<s:if test="user.profilePicture == null">
								<img src="images/avatar.png" style="width: 100px;">
							</s:if>
							<s:else>
								<img src="${user.profilePicture}" style="width: 170px; height:190px;">
							</s:else>
							</div>
							<div class="col-sm-10 text-left">
								<div class="col-sm-12">
									<a href="">
										<h3>${title.toUpperCase()}</h3>
									</a>
								</div>
								<div class="col-sm-12">
									<p>
										${content}...
									</p>
									<p>
										<a href="detail-thread?q=${threadId}" class="btn btn-success" style="width:120px;">Read More</a>
									</p>
								</div>
							</div>
						</div>

	


					</div>
				</div>
			</div>
			<!-- end of list -->
		</s:iterator>




		</div>
	</div>

 <s:include value="/WEB-INF/footer/footer.jsp"/>

</body>
</html>