<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
<title>ABC Jobs Portal - Welcome</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<script src="https://cdn.ckeditor.com/4.15.0/standard-all/ckeditor.js"></script>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="css/style.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
<style>
.card {
	width: 25rem;
	text-align: left;
}

body {
	font-size: 14px;
}
</style>
</head>

<body>
	<s:if test="#session.isLoggedIn != null">
		<s:include value="/WEB-INF/header/header.jsp"/>
	</s:if>
	<s:else>
		<s:include value="/WEB-INF/header/LoginHeader.jsp"/>
	</s:else>
	<div class="container fluid mt-3" style="min-height: 500px;">
		<div class="row">
			<div class="col-sm-12">
				<h3>Post a thread</h3>
			<s:if test="msg == 'true'">
				<div class="alert alert-info" role="alert">
  					You have successfully posted a thread!
				</div>
			</s:if>
			</div>
			<!-- title -->
			<div class="col-sm-12 col-md-12 col-lg-12">
			<form action="process-thread-post" method="POST">	
				<div class="form-group mb-4">
					<input type="text" class="form-control" name="thread.title"
						aria-describedby="emailHelp" placeholder="Title">
				</div>
			
			<!-- end of title  -->
			<!-- content -->
			

			<div class="form-group mb-4">
			 	<textarea class="form-control"  name="thread.content" id="editor" required>		
        	 	</textarea>	
			</div>

	
			<!-- end of content -->
			<div class="col-sm-12 mt-2 text-right">
				<button class="btn btn-primary" type="submit" style="width: 100px;">Post</button>
			</div>
			</form>	
			
			</div>
		</div>
	</div>

 <s:include value="/WEB-INF/footer/footer.jsp"/>

   <script>
	    CKEDITOR.replace('editor', {
	      width: '100%',
	      height: 260
	    });
  </script>
</body>
</html>