<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
    if(session.getAttribute("username") == null) {
       response.sendRedirect("login.jsp");
       return ;
    }
%>
<!DOCTYPE html>
<html lang="en">
<head>
<title>ABC Jobs Portal - Welcome</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<script
	src="https://cdn.ckeditor.com/ckeditor5/23.0.0/inline/ckeditor.js"></script>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="css/style.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

</head>

<body>

	<s:include value="/WEB-INF/header/header.jsp"/>
	
	<div class="container-fluid mt-3">
		<div class="row">
			<div class="col-sm-12 col-md-8 col-lg-4 " style="margin: auto;">
				<s:if test="successMsg == 'true'">
					<div class="alert alert-success text-center" role="alert" >
  					
						<p>Your profile is successfully updated!</p>
					
					</div>
				 </s:if>
				<h4 class="text-info">Update Profile</h4>
				<form action='update-process' method='post' enctype="multipart/form-data">
					<div class="form-group mb-3 ">
						<s:if test="#session.userProfile == null">
								<img src="images/avatar.png" class="mb-3"
								style="width:150px; height:180px; float: left;">
						</s:if>
						<s:else>
							
								<img src="${user.profilePicture}"  class="mb-3"
								style="width:150px; height:180px; float: left;">
							
						
						</s:else>
						
						<div style="position: relative; top: 50px;">
							<input type="file" class="form-control-file"
								name="myFile" style="width: 20%;">
						</div>
					</div>
					<div class="form-group mb-3">
						<select class="form-control" name='user.country'>
							<option value='0'>Select Country</option>
							<s:iterator value="countryList">
								<s:if test="user.country == countryId">
									<option value="${countryId}" selected>${countryName}</option>
								</s:if>
								<s:else>
									<option value="${countryId}">${countryName}</option>
								</s:else>
							</s:iterator>
						
							
						</select>
					</div>
					<div class="form-group mb-3">
						<input type="text" class="form-control" id="exampleInputEmail1"
							 value="${user.username}" placeholder="Username" disabled>
					</div>
					<div class="form-group mb-3">

						<input type="text" class="form-control" id="exampleInputEmail1"
							name="user.firstName" value="${user.firstName}" placeholder="First Name" >
					</div>
					<div class="form-group mb-3">

						<input type="text" class="form-control" id="exampleInputEmail1"
							name="user.lastName" value="${user.lastName}" placeholder="Last Name">
					</div>
					<div class="form-group mb-3">
						<input type="email" class="form-control" id="exampleInputEmail1"
							value="${user.email}" placeholder="email" disabled>
					</div>

					<div class="form-group mb-4">

						<input type="password" class="form-control"
							name="user.password" placeholder="Password">
					</div>
					<button type="submit" class="btn btn-md btn-primary"
						style="width: 100%;">Save & Update</button>
				</form>
			</div>

		</div>
	</div>

 <s:include value="/WEB-INF/footer/footer.jsp"/>

</body>
</html>