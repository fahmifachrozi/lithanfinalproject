
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
<title>ABC Jobs Portal - Welcome</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">

<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="css/style.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
<style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
.card {
	width: 25rem;
	text-align: left;
}

body {
	font-size: 14px;
}
</style>
</head>

<body>
	
	<s:if test="#session.isLoggedIn != null">
		<s:include value="/WEB-INF/header/header.jsp"/>
	</s:if>
	<s:else>
		<s:include value="/WEB-INF/header/LoginHeader.jsp"/>
	</s:else>

	<div class="container fluid mt-3" style="min-height: 500px;">
		<div class="row">
			<h2>User Management</h2>
		
			<div class="col-sm-12">
	
				<table class="table">
					<caption>List of users</caption>
					<thead>
						<tr>
							<th scope="col">#</th>
							<th scope="col">Profile Picture</th>
							<th scope="col">Role Access</th>
							<th scope="col">Username</th>
							<th scope="col">First Name</th>
							<th scope="col">Last Name</th>
							<th scope="col">Email</th>
							<th scope="col">Status</th>
							<th scope="col"></th>
							
						</tr>
					</thead>
					<tbody>
					<s:iterator value="listUser" status="testStatus">
						<tr>
							<th scope="row"><s:property value="#testStatus.count"/></th>
							<s:if test="profilePicture != null">
								<td><img src="<s:property value="profilePicture" />" style="width:60px; height:60px;"></td>
							</s:if>
							<s:else>
								<td><img src="images/avatar.png" style="width:60px; height:60px;"></td>
							</s:else>
							<s:if test="role_id == 1">
								<td>Software Programmer</td>
							</s:if>
							<s:else>
								<td>Administrator</td>
							</s:else>
						
							<td>${username}</td>
							<td>${firstName}</td>
							<td>${lastName}</td>
							<td>${email}</td>
							
							<s:if test="suspended == 1">
								<td><b>SUSPENDED</b></td>
							</s:if>
							<s:else>
								<td><b>ACTIVE</b></td>
							</s:else>
							<td>
							<s:if test="suspended != 1 && role_id == 1">
								
								<a href="javascript:void(0)" class="btn btn-danger suspend" data-toggle="modal" data-target="#suspendModal" data-suspend="${username}" >Suspend</a>
							</s:if>
							</td>
						</tr>
					</s:iterator>
					</tbody>
				</table>
			</div>

		</div>
	</div>
	
		<!-- Modal -->
<div class="modal fade" id="suspendModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">

      <div class="modal-body">
       	<h4>Are you sure you want to suspend this user?</h4>
      </div>
      <div class="modal-footer">
        <a type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</a>
        <a  id="suspend-btn" type="button" class="btn btn-danger">Suspend</a>
      </div>
    </div>
  </div>
</div>


<script>
$(document).ready(function(){

	  // jQuery methods go here...
		
	$(".suspend").on('click', function() {
		
		  let suspendVal = $(this).data("suspend");
		  $("#suspend-btn").attr("href", "suspend-user?username="+suspendVal);
		  
		});
	});

</script>

 <s:include value="/WEB-INF/footer/footer.jsp"/>

</body>
</html>