<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
<nav class="navbar navbar-expand-sm bg-dark navbar-dark">
	<a href="index.jsp">
		<h2 class="navbar-brand text-white">
			<img src="images/logo.png" style="width: 50px; height: auto;">
			Jobs
		</h2>
	</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse"
			data-target="#collapsibleNavbar">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse" id="collapsibleNavbar">
			<ul class="navbar-nav ml-auto">
				<li class="nav-item mr-3 mt-1">
					<form class="example" action="search" method="GET">
						<input type="text" placeholder="Search.." name="query">
						<button type="submit">
							<i class="fa fa-search text-secondary"></i>
						</button>
					</form>
				</li>
				<li class="nav-item"><a href="register.jsp"
					class="btn btn-success mr-1 regist-btn mt-2" >Register</a></li>
				<li class="nav-item"><a href="login.jsp"
					class="btn btn-primary login-btn mt-2">Login</a></li>

			</ul>
		</div>
	</nav>