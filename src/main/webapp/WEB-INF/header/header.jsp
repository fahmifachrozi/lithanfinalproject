<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<nav class="navbar navbar-expand-sm bg-dark navbar-dark">
		<h2 class="navbar-brand text-white">
			<s:if test="#session.isLoggedIn != true">
				<a href="index.jsp" style="color: white; text-decoration: none;">
					<img src="images/logo.png" style="width: 50px; height: auto;">
					Jobs
				</a>
			</s:if>
			<s:else>
				<a href="profile?username=${session.username}" style="color: white; text-decoration: none;">
					<img src="images/logo.png" style="width: 50px; height: auto;">
					Jobs
				</a>
			</s:else>
		
		</h2>

		<button class="navbar-toggler" type="button" data-toggle="collapse"
			data-target="#collapsibleNavbar">
			<span class="navbar-toggler-icon"></span>
		</button>

		<div class="collapse navbar-collapse" id="collapsibleNavbar">
			<ul class="navbar-nav ml-auto ">
				<li class="nav-item mr-3 mt-1">
					<form class="example" action="search" method="GET">
						<input type="text" placeholder="Search.." name="query">
						<button type="submit">
							<i class="fa fa-search text-secondary"></i>
						</button>
					</form>
				</li>
				
			<s:if test="#session.role_id == 2">
				<li class="nav-item mr-3 mt-1">
					<div class="text-center">
					<a href="user-lists">
						<i class="fa fa-users text-white" style="font-size: 25px"></i><br>
						<span class="text-white" style="font-size: 12px;">User Management</span>
					</a>
					</div>
				</li>
			</s:if>
			
				<li class="nav-item mr-3 mt-1">
					<div class="text-center">
					<a href="message-lists">
						<i class="fa fa-envelope-o text-white" style="font-size: 25px"></i><br>
						<span class="text-white" style="font-size: 12px;">Messaging</span>
					</a>
					</div>
				</li>
	
				<li class="nav-item mr-3 ">
					<div class="text-center">
						<a href="javascript:void(0)" data-toggle="modal" data-target="#exampleModal"> 
					<s:if test="#session.profile_picture == null">
						<img src="images/avatar.png"
							class="rounded-circle"
							style="height: 30px; width: auto; display: inline;">
					</s:if>
					<s:else>
						<img src="${session.profile_picture}"
							class="rounded-circle"
							style="height: 30px; width: 30px; display: inline; margin-top:5px;">
					</s:else>	
							<br>
							<span class="text-white" style="font-size: 12px; display: block;">Me</span>
						</a>
					</div>
				</li>

			</ul>
		</div>
	</nav>