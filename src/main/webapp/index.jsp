<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
<title>ABC Jobs Portal - Welcome</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
	<link rel="stylesheet" href="css/style.css">
<style>
@media only screen and (max-width: 768px) {
	/* For mobile phones: */
	.regist-btn {
		margin-bottom: 10px;
		width: 100px;
	}
	.login-btn {
		width: 100px;
	}
}
</style>
</head>

<body>
	 <s:include value="/WEB-INF/header/LoginHeader.jsp"/>
	 
	<div class="container" style="min-height: 450px;">
		<div class="row mt-5">
			<div class="col-sm-12 col-md-8 col-lg-8">
				<img src="images/website-banner.jpg"
					style="width: 100%; height: auto;">
			</div>
			<div class="col-sm-12 col-md-4 col-lg-4">
				<h1 class="text-info">Welcome to ABC Jobs Portal</h1>

				<form action="login" method="post">
					<div class="form-group">

						<input type="text" class="form-control" name="usernameOrEmail"
							aria-describedby="emailHelp" placeholder="Enter email">

					</div>
					<div class="form-group">

						<input type="password" class="form-control"
							 name="password" placeholder="Password">
					</div>
					<button type="submit" class="btn btn-primary" style="width: 100%;">Login</button>
				</form>
				<hr>
				<a href="reset-password.jsp">Forgot Password?</a>

			</div>
		</div>
	</div>

 <s:include value="/WEB-INF/footer/footer.jsp"/>

</body>
</html>