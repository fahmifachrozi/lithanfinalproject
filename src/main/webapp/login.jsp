<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
<title>ABC Jobs Portal - Login</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

</head>

<body>

	<div class="container mt-3" style="min-height: 550px;">
		<div class="row">
			<div class="col-sm-12">
				<a href="index.jsp" class="text-secondary"
					style="font-size: 2rem; text-decoration: none;"> <i
					class="fa fa-chevron-circle-left"></i> Index
				</a>
			</div>
		</div>
		<div class="row mt-5">
			<div class="col-sm-12 text-center">
				<h2>
					<img src="images/logo.png" style="width: 50px; height: auto;">
					Jobs Portal
				</h2>
			</div>
			<div class="col-sm-6 col-md-6 col-lg-6" style="margin: auto;">
				<s:if test="errorMsg.length() > 0">
					<div class="alert alert-danger text-center" role="alert" >
  					
						${errorMsg}
					
					</div>
				 </s:if>
			
				<h4 class="text-info">Login</h4>
				<form action="login" method="post">
					<div class="form-group mb-4">

						<input type="text" class="form-control" name="usernameOrEmail"
							aria-describedby="emailHelp" placeholder="Enter Username or Email">
					</div>
					<div class="form-group mb-5">

						<input type="password" class="form-control" name="password"
							 placeholder="Password">
					</div>
					<button type="submit" class="btn btn-lg btn-primary"
						style="width: 100%;">Login</button>
				</form>
				<hr>
				<a href="reset-password.jsp">Forgot Password?</a> | <a
					href="register.jsp">Register</a>

			</div>
		</div>
	</div>

 <s:include value="/WEB-INF/footer/footer.jsp"/>

</body>
</html>