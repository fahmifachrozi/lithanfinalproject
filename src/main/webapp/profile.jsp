<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html lang="en">
<head>
<title>ABC Jobs Portal - Welcome</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">

<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="css/style.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>


</head>

<body>

	<s:if test="#session.isLoggedIn != null">
		<s:include value="/WEB-INF/header/header.jsp"/>
	</s:if>
	<s:else>
		<s:include value="/WEB-INF/header/LoginHeader.jsp"/>
	</s:else>
	 	
	
	<div class="container-fluid mt-3">
		<div class="row">
			<div class="col-sm-12 col-md-12 col-lg-3">
				<div class="card">
					<s:if test="#session.username == user2.username">
						<a href="update-profile" class="text-right"
						style="font-size: 24px; margin: 5px;"> <i
						class="fa fa-gear text-right"></i>
						</a>
						<h4>My Profile</h4>
					</s:if>	 
					<s:else>
						<br>
					</s:else>
					<s:if test="user.profilePicture == null ">
					<img src="images/avatar.png" alt=""
						style="width: 200px; height:220px; margin: auto;">
						
					</s:if>
					<s:else>
						<img src="${user.profilePicture}" alt=""
						style="width: 200px; height:220px; margin: auto;">
					</s:else>
					<br>
					<h5>${user.firstName.toUpperCase()} ${user.lastName.toUpperCase()}</h5>
					<s:if test="#session.role_id == 1">
						<p class="title">Software Programmer</p>
					</s:if>
						
					<s:else>
						<p class="title">Administrator</p>
					</s:else>
					<p>${user.countryName.toUpperCase()}</p>
				
					<p>
					<s:if test="#session.isLoggedIn">
						<s:if test="#session.username == user.username">
							<a href="update-profile" class="btn btn-success">Edit Profile</a>
						</s:if>
						<s:else>
							<a href="message-history?friendId=${user.user_id}" class="btn btn-primary" style="width:100px;">
								<i class="fa fa-comments-o" style=" font-size: 1.5rem;"></i>
							</a>
						</s:else>
					</s:if>
					</p>
				</div>
				
				
			</div>

			<div class="col-sm-12 col-md-12 col-lg-5  mt-4 mb-5">
				<div class="row">
				
				<s:if test="#session.username == username">
					<div class="col-sm-12 col-md-12 col-lg-12 text-right mb-4">
						<a href="job-post" class="btn btn-danger">Post a Job</a>
						<a href="thread-post" class="btn btn-danger">Post a Thread</a>
					</div>
				</s:if>
	
	<s:if test="threadList.size() > 0">	
	<s:iterator value="threadList">
					<div class="col-sm-12 col-md-12 col-lg-12 mt-2">
						<div class=" p-2 mb-2 "
							style="width: 100%; border: 1px solid grey;">

							<a href="detail-thread?q=${threadId}"><h5>${title.toUpperCase()}</h5></a>

							<div>
								<span>posted on: ${post_date }</span>
							</div>
						
							<hr>
							
							<p>
							${content}
							</p>
							
						</div>

					</div>
	</s:iterator>
	</s:if>
	<s:else>
					<div class="col-sm-12 col-md-12 col-lg-12 mt-2 text-center">
							<div class=" p-2 mb-2 "
							style="width: 100%; border: 1px solid grey;">

							<h5>No Post</h5>

							<div>
							</div>
							
							</div>
						</div>
						
						
	</s:else>



				</div>
			</div>

			<!-- start of jobs card -->
			<div class="col-sm-12 col-md-12 col-lg-4 ">
				<div class="card text-left p-2 mb-2 text-info"
					style="max-width: 100%;">
					<h5>Recent Jobs</h5>
					<ul class="list-unstyled p-2">
					
					
					<s:iterator value="jobRecentList">
						<li class="mb-2">
							<s:if test="user.profilePicture == null">
								<img src="images/avatar.png" class="img-list rounded-circle">
							</s:if>
							<s:else>
								<img src="${user.profilePicture}" class="img-list rounded-circle" style="width:35px; height:35px;">
							</s:else>
							<div class="title" >
								<a href="detail-job?q=${jobId}" style="font-size:14px;">${jobTitle.toUpperCase()}</a>
							</div>
							<div class="title">
								<span> Posted by: ${user.firstName} ${user.lastName}</span>
							</div>
								<div class="title">
								<span><b>Due date: ${end_date}</b></span>
							</div>
							
						</li>
					</s:iterator>
					
					
					</ul>
					<div class="col-sm-12 text-right">
						<a href="job-lists"
							style="font-size: 14px; color: blue;">See More..</a>
					</div>
				</div>
				<div class="card text-left p-2 text-success mb-2"
					style="width: 100%;">
					<h5>Recent Thread</h5>
					<ul class="list-unstyled p-2">
					<s:iterator value="threadRecentList">
						<li class="mb-2">
							<s:if test="user.profilePicture == null">
								<img src="images/avatar.png" class="img-list rounded-circle">
							</s:if>
							<s:else>
								<img src="${user.profilePicture}" class="img-list rounded-circle" style="width:35px; height:35px;">
							</s:else>
							<div class="title">
								<a href="detail-thread?q=${threadId}" style="font-size:14px;">${title.toUpperCase()}</a>
							</div>
							<div class="title">
								<span> Posted by: ${user.firstName} ${user.lastName}</span>
							</div>
							
						</li>
					</s:iterator>

					</ul>
					<div class="col-sm-12 text-right">
						<a href="thread-lists" style="font-size: 14px; color: blue;">See
							More..
						</a>
					</div>

				</div>

			</div>
			<!-- end of job card -->

		</div>
	</div>




 <s:include value="/WEB-INF/footer/footer.jsp"/>


</body>
</html>