<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
<title>ABC Jobs Portal - Registration</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

</head>

<body>

	<div class="container mt-3">
		<div class="row">
		
			<div class="col-sm-12">
				<a href="index.jsp" class="text-secondary"
					style="font-size: 2rem; text-decoration: none;"> <i
					class="fa fa-chevron-circle-left"></i> Index
				</a>
			</div>
		</div>
			
		<div class="row mt-5">
			<div class="col-sm-12 text-center">
				<h2>
					<img src="images/logo.png" style="width: 50px; height: auto;">
					Jobs Portal
				</h2>
			</div>
			<div class="col-sm-6 col-md-6 col-lg-6" style="margin: auto;">
				<s:if test="errorMsg.size() > 0">
					<div class="alert alert-danger text-center" role="alert" >
  					
						${errorMsg.get(0)}
					
					</div>
				 </s:if>
				<h4 class="text-info">Registration</h4>

			

				<form action="register" method="post">
					<div class="form-group mb-4">
						<input type="text" class="form-control" name="user.username" value="${user.username}"
							aria-describedby="emailHelp" placeholder="Enter Username">
					</div>
					<div class="form-group mb-4">

						<input type="text" class="form-control" name="user.firstName" value="${user.firstName}"
							aria-describedby="emailHelp" placeholder="Enter First Name">
					</div>
					<div class="form-group mb-4">

						<input type="text" class="form-control" name="user.lastName" value="${user.lastName}"
							aria-describedby="emailHelp" placeholder="Enter Last Name">
					</div>
					<div class="form-group mb-4">

						<input type="email" class="form-control" name="user.email" value="${user.email}"
							aria-describedby="emailHelp" placeholder="Enter email"> <small
							id="emailHelp" class="form-text text-muted">We'll never
							share your email with anyone else.</small>

					</div>
					<div class="form-group mb-5">

						<input type="password" class="form-control" name="user.password"
							placeholder="Password">
					</div>
					<button type="submit" class="btn btn-lg btn-primary"
						style="width: 100%;">Sign Up</button>
				</form>
				<hr>
				<a href="reset-password.jsp">Forgot Password?</a> | <a
					href="login.jsp">Login</a>

			</div>
		</div>
	</div>

 <s:include value="/WEB-INF/footer/footer.jsp"/>

</body>
</html>