<!DOCTYPE html>
<html lang="en">
<head>
<title>ABC Jobs Portal - Reset Password</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

</head>

<body>

	<div class="container mt-3" style="min-height: 550px;">
		<div class="row">
			<div class="col-sm-12">
				<a href="index.jsp" class="text-secondary"
					style="font-size: 2rem; text-decoration: none;"> <i
					class="fa fa-chevron-circle-left"></i> Index
				</a>
			</div>
		</div>
		<div class="row mt-5">
			<div class="col-sm-12 text-center">
				<h2>
					<img src="images/logo.png" style="width: 50px; height: auto;">
					Jobs Portal
				</h2>
			</div>
			<div class="col-sm-6 col-md-6 col-lg-6" style="margin: auto;">
				<h4 class="text-info">Reset Password</h4>
				<form action="reset-password" method="POST">
					<div class="form-group mb-4">
						<input type="email" class="form-control" name="email"
							aria-describedby="emailHelp" placeholder="Enter email" required>
					</div>

					<button type="submit" class="btn btn-lg btn-primary"
						style="width: 100%;">Reset Password</button>
				</form>
				<hr>
				<a href="login.jsp">Login</a> | <a href="register.jsp">Register</a>

			</div>
		</div>
	</div>

	 <s:include value="/WEB-INF/footer/footer.jsp"/>
</body>
</html>