package com.action.test;

import org.apache.struts2.StrutsTestCase;
import org.junit.Test;

import com.action.RegistrationAction;
import com.opensymphony.xwork2.ActionProxy;
import com.opensymphony.xwork2.ActionSupport;

public class RegistrationActionTest extends StrutsTestCase {

	@Test
	public void testNoData() throws Exception{
		
		ActionProxy actionProxy = getActionProxy("register");
		RegistrationAction action = (RegistrationAction) actionProxy.getAction();
		assertNotNull("The action is null but should not be.", action);
		String result = actionProxy.execute();
		assertEquals("The execute method did not return success but should have.", "failed", result);
	}
	
	@Test
	public void testExecuteValidationPasses() throws Exception{
		
		request.setParameter("user.username", "johnwick");
		request.setParameter("user.firstName", "john");
		request.setParameter("user.lastName", "wick");
		request.setParameter("user.email", "johnwick@gmail.com");
		request.setParameter("user.password", "wick12345");
		
		ActionProxy actionProxy = getActionProxy("register");
		RegistrationAction action = (RegistrationAction) actionProxy.getAction();
		assertNotNull("The action is null but should not be.", action);
		String result = actionProxy.execute();
		assertEquals("The execute method did not return success but should have.", "success", result);
	}
	
	@Test
	public void testPasswordLessThanSix() throws Exception{
		
		request.setParameter("user.username", "johnwick");
		request.setParameter("user.firstName", "john");
		request.setParameter("user.lastName", "wick");
		request.setParameter("user.email", "johnwick@gmail.com");
		request.setParameter("user.password", "wick1");
		
		ActionProxy actionProxy = getActionProxy("register");
		RegistrationAction action = (RegistrationAction) actionProxy.getAction();
		assertNotNull("The action is null but should not be.", action);
		String result = actionProxy.execute();
		assertEquals("The execute method did not return success but should have.", "input", result);
	}
	
}
