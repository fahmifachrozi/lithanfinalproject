<!DOCTYPE html>
<html lang="en">
<head>
<title>ABC Jobs Portal - Welcome</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<script
	src="https://cdn.ckeditor.com/ckeditor5/23.0.0/inline/ckeditor.js"></script>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="css/style.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

</head>

<body>
	<nav class="navbar navbar-expand-sm bg-dark navbar-dark">
		<h2 class="navbar-brand text-white">
			<img src="images/logo.png" style="width: 50px; height: auto;">
			Jobs
		</h2>

		<button class="navbar-toggler" type="button" data-toggle="collapse"
			data-target="#collapsibleNavbar">
			<span class="navbar-toggler-icon"></span>
		</button>

		<div class="collapse navbar-collapse" id="collapsibleNavbar">
			<ul class="navbar-nav ml-auto ">
				<li class="nav-item mr-3 mt-1">
					<form class="example" action="action_page.php">
						<input type="text" placeholder="Search.." name="search">
						<button type="submit">
							<i class="fa fa-search text-secondary"></i>
						</button>
					</form>
				</li>
				<li class="nav-item mr-3 mt-1"><a href="MessageList.html">
						<div class="text-center">
							<i class="fa fa-envelope-o text-white" style="font-size: 25px"></i><br>
							<span class="text-white" style="font-size: 12px;">Messaging</span>
						</div>
				</a></li>
				<li class="nav-item mr-3 mt-1">
					<div class="text-center">
						<i class="fa fa-bell-o text-white" style="font-size: 25px"></i><br>
						<span class="text-white" style="font-size: 12px;">Notification</span>
					</div>
				</li>
				<li class="nav-item mr-3 ">
					<div class="text-center">
						<a href="#"> <img src="images/avatar.png"
							class="rounded-circle"
							style="height: 30px; width: auto; display: inline;"><br>
							<span class="text-white" style="font-size: 12px; display: block;">Me</span>
						</a>
					</div>
				</li>

			</ul>
		</div>
	</nav>
	<div class="container-fluid mt-3">
		<div class="row">
			<div class="col-sm-12 col-md-12 col-lg-3">
				<div class="card">

					<a href="UpdateProfile.html" class="text-right"
						style="font-size: 24px; margin: 5px;"> <i
						class="fa fa-gear text-right"></i>
					</a> <img src="images/avatar.png" alt=""
						style="width: 80%; margin: auto;">
					<h3>John Doe</h3>
					<p class="title">CEO & Founder, Example</p>
					<p>Harvard University</p>
					<div style="margin: 24px 0;">
						<a href="#"><i class="fa fa-dribbble"></i></a> <a href="#"><i
							class="fa fa-twitter"></i></a> <a href="#"><i
							class="fa fa-linkedin"></i></a> <a href="#"><i
							class="fa fa-facebook"></i></a>
					</div>
					<p>
						<button class="btn-success">Follow</button>
					</p>
				</div>
				<div class="card mt-3 text-left p-2 mb-3">
					<h5 class="text-success">My Circle</h5>

					<div class="row mt-2">
						<div class="col-sm-6 col-md-6 col-lg-4 text-center">
							<img src="images/avatar.png" style="width: 100%;"> <br>
							<span>Andi</span>
						</div>
						<div class="col-sm-6 col-md-6 col-lg-4 text-center">
							<img src="images/avatar.png" style="width: 100%;"> <br>
							<span>Andi</span>
						</div>
						<div class="col-sm-6 col-md-6 col-lg-4 text-center">
							<img src="images/avatar.png" style="width: 100%;"> <br>
							<span>Andi</span>
						</div>
						<div class="col-sm-6 col-md-6 col-lg-4 text-center">
							<img src="images/avatar.png" style="width: 100%;"> <br>
							<span>Andi</span>
						</div>
						<div class="col-sm-6 col-md-6 col-lg-4 text-center">
							<img src="images/avatar.png" style="width: 100%;"> <br>
							<span>Andi</span>
						</div>
					</div>
				</div>
			</div>

			<div class="col-sm-12 col-md-12 col-lg-5  mt-4 mb-5">
				<div class="row">
					<div class="col-sm-12 col-md-12 col-lg-12 text-right mb-4">
						<h6>What do you think?</h6>
						<div style="background-color: #e6e6e6;">

							<div id="editor"></div>

						</div>
						<button class="btn btn-info" style="width: 20%; margin-top: 2%;">Post</button>
					</div>


					<div class="col-sm-12 col-md-12 col-lg-12 mt-2">
						<div class=" p-2 mb-2 "
							style="width: 100%; border: 1px solid grey;">

							<img src="images/avatar.png" class="rounded-circle"
								style="width: 50px; float: left;">

							<div>
								<span>Fahmi Fachrozi</span>
							</div>
							<div>
								<span>Indonesia</span>
							</div>

							<hr>
							<p>Lorem Ipsum is simply dummy text of the printing and
								typesetting industry. Lorem Ipsum has been the industry's
								standard dummy text ever since the 1500s, when an unknown
								printer took a galley of type and scrambled it to make a type
								specimen book. It has survived not only five centuries, but also
								the leap into electronic typesetting, remaining essentially
								unchanged. It was popularised in the 1960s with the release of
								Letraset sheets containing Lorem Ipsum passag</p>
						</div>

					</div>

					<div class="col-sm-12 col-md-12 col-lg-12 mt-2">
						<div class=" p-2 mb-2 "
							style="width: 100%; border: 1px solid grey;">

							<img src="images/avatar.png" class="rounded-circle"
								style="width: 50px; float: left;">

							<div>
								<span>Fahmi Fachrozi</span>
							</div>
							<div>
								<span>Indonesia</span>
							</div>

							<hr>
							<p>Lorem Ipsum is simply dummy text of the printing and
								typesetting industry. Lorem Ipsum has been the industry's
								standard dummy text ever since the 1500s, when an unknown
								printer took a galley of type and scrambled it to make a type
								specimen book. It has survived not only five centuries, but also
								the leap into electronic typesetting, remaining essentially
								unchanged. It was popularised in the 1960s with the release of
								Letraset sheets containing Lorem Ipsum passag</p>
						</div>

					</div>

				</div>
			</div>

			<!-- start of jobs card -->
			<div class="col-sm-12 col-md-12 col-lg-4 ">
				<div class="card text-left p-2 mb-2 text-info"
					style="max-width: 100%;">
					<h6>Jobs</h6>
					<ul class="list-unstyled p-2">
						<li class="mb-2"><img src="images/avatar.png"
							class="img-list rounded-circle">
							<div class="title">
								<a href="#">asdfasdasd</a>
							</div>
							<div class="title">
								<span> Posted by: Fahmi </span>
							</div></li>
						<li><img src="images/avatar.png"
							class="img-list rounded-circle">
							<div class="title">
								<a href="#">asdfasdasd</a>
							</div>
							<div class="title">
								<span> Posted by: Fahmi </span>
							</div></li>
					</ul>
					<div class="col-sm-12 text-right">
						<a href="JobOpportunityList.html"
							style="font-size: 14px; color: blue;">See More..</a>
					</div>
				</div>
				<div class="card text-left p-2 text-success mb-2"
					style="width: 100%;">
					<h6>Thread</h6>
					<ul class="list-unstyled p-2">
						<li class="mb-2"><img src="images/avatar.png"
							class="img-list rounded-circle">
							<div class="title">
								<a href="#">asdfasdasd</a>
							</div>
							<div class="title">
								<span> Posted by: Fahmi </span>
							</div></li>
						<li><img src="images/avatar.png"
							class="img-list rounded-circle">
							<div class="title">
								<a href="#">asdfasdasd</a>
							</div>
							<div class="title">
								<span> Posted by: Fahmi </span>
							</div></li>

					</ul>
					<div class="col-sm-12 text-right">
						<a href="ThreadList.html" style="font-size: 14px; color: blue;">See
							More..</a>
					</div>

				</div>

			</div>
			<!-- end of job card -->

		</div>
	</div>

	<!-- footer -->
	<div class="container-fluid bg-secondary text-white mt-5">
		<div class="row">
			<div class="col-sm-12 text-center p-4">
				<p>Copyright &copy; M. Fahmi Fachrozi</p>
			</div>
		</div>
	</div>

	<script>
  InlineEditor
      .create( document.querySelector( '#editor' ) )
      .catch( error => {
          console.error( error );
      } );
</script>
</body>
</html>